
from src.graph_utilities.graph_builder import *

myid = ID_Generator()

def create_mapping_of_names_to_nodes(assembly_tiers):
    bill_of_materials = []
    for tier in assembly_tiers:
        for comp in tier:
            bill_of_materials.append(comp)
    mapping = {}
    for i, comp in enumerate(bill_of_materials):
        mapping[i] = comp
    return mapping

def create_subset_sizes(assembly_tiers):
    subset_sizes = []
    for tier in assembly_tiers:
        subset_sizes.append(len(tier))
    return subset_sizes

def create_contact_graph(contact_matrix_df):
    g = nx.from_numpy_matrix(contact_matrix_df.values, parallel_edges=True, create_using=nx.Graph())
    label_mapping = {idx: val for idx, val in enumerate(contact_matrix_df.columns)}
    g = nx.relabel_nodes(g, label_mapping)
    return g

def create_precedence_graph(contact_graph, assembly_tiers):
    G = nx.DiGraph()
    for tier in range(len(assembly_tiers)):
        for part in assembly_tiers[tier]:
            G.add_node(part, tier=tier)
    for u,v,a in contact_graph.edges(data=True):
        #print(u, G.nodes[u]['tier'], v, G.nodes[v]['tier'])
        if (G.nodes[u]['tier'] - G.nodes[v]['tier'] == -1):
            G.add_edge(u,v)
        if (G.nodes[v]['tier'] - G.nodes[u]['tier'] == -1):
            G.add_edge(v,u)
    return G

def create_multilayered_graph(*subset_sizes):
    extents = pairwise(itertools.accumulate((0,) + subset_sizes))
    layers = [range(start, end) for start, end in extents]
    G = nx.DiGraph()
    for (i, layer) in enumerate(layers):
        G.add_nodes_from(layer, layer=i)
    for layer1, layer2 in pairwise(layers):
        G.add_edges_from(itertools.product(layer1, layer2))
    return G

def parts_not_connected(contact_matrix_df, u, v):
    if contact_matrix_df.loc[u, v] == 0:
        return True
    else:
        return False

# -------------------------
# Decision tree (AOG -> PG)
# -------------------------

def get_decision_tree(AOG, flipped_reduced_AOG, flipped_duplicates, simple_nodes):
    decision_tree = nx.DiGraph()
    decision_tree = add_nodes_with_edges(decision_tree, AOG, flipped_reduced_AOG, flipped_duplicates)
    decision_tree = add_start_node_with_edges(decision_tree, flipped_duplicates)
    remove_all_simple_nodes_from_end(decision_tree, simple_nodes)
    return decision_tree

def add_nodes_with_edges(decision_tree, AOG, flipped_reduced_AOG, flipped_duplicates):
    max_num_keys_of_AOG = len(AOG)
    print('add_nodes_with_edges:')
    for key, value in AOG.items():
        #print(f'add_nodes_with_edges: {key}/{max_num_keys_of_AOG}', end="\r", flush=True)
        sys.stdout.write('\r' + str(key) + '/' + str(max_num_keys_of_AOG)+ ' len_of_decision_tree: ' + str(len(decision_tree)))
        sys.stdout.flush()
        u_label = key
        sub_1 = value[1]
        sub_2 = value[2]
        if len(sub_1) > 1:
            add_simple_edge_to_decision_tree(sub_1, flipped_reduced_AOG, decision_tree, u_label)
            add_decision_edges_to_decision_tree(sub_1, flipped_duplicates, decision_tree, u_label)
        if len(sub_2) > 1:
            add_simple_edge_to_decision_tree(sub_2, flipped_reduced_AOG, decision_tree, u_label)
            add_decision_edges_to_decision_tree(sub_2, flipped_duplicates, decision_tree, u_label)
    return decision_tree

def add_start_node_with_edges(decision_tree, flipped_duplicates):
    start_node_id = myid.generate_id()
    decision_tree.add_node(start_node_id, label='Start')
    first_key = list(flipped_duplicates.keys())[0]

    # get uuids of nodes with first_key
    uuids_of_first_key = get_uuids_of_first_key(decision_tree, first_key, flipped_duplicates)
    for node in uuids_of_first_key:
        decision_tree.add_edge(start_node_id, node)
    return decision_tree

def get_uuids_of_first_key(decision_tree, first_key, flipped_duplicates):
    uuids_of_first_key = []
    for val in flipped_duplicates[first_key]:
        for node, data in decision_tree.nodes(data=True):
            if data['label'] == val:
                uuids_of_first_key.append(node)
    return uuids_of_first_key

def remove_all_simple_nodes_from_end(decision_tree, simple_nodes):
    nodes_to_remove = get_simple_nodes_to_remove(decision_tree, simple_nodes)
    while nodes_to_remove != []:
        for node in nodes_to_remove:
            decision_tree.remove_node(node)
        nodes_to_remove = get_simple_nodes_to_remove(decision_tree, simple_nodes)

def get_simple_nodes_to_remove(decision_tree, simple_nodes):
    nodes_to_remove = []
    for node, data in decision_tree.nodes(data=True):
        if decision_tree.out_degree(node) == 0:
            if data['label'] in simple_nodes:
                nodes_to_remove.append(node)
    return nodes_to_remove

def add_simple_edge_to_decision_tree(sub, flipped_reduced_AOG, decision_tree, u_label):
    if str(sub) in list(flipped_reduced_AOG.keys()):
        existing_u = append_existing_u(decision_tree, u_label)
        if existing_u == []:
            u = myid.generate_id()
            decision_tree.add_node(u, label=u_label)
            existing_u.append(u)

        for u_elem in existing_u:
            process_for_sub = flipped_reduced_AOG[str(sub)]
            v = process_for_sub
            node_id = myid.generate_id()
            decision_tree.add_node(node_id, label=v)
            decision_tree.add_edge(u_elem, node_id)

def add_decision_edges_to_decision_tree(sub, flipped_duplicates, decision_tree, u_label):
    if str(sub) in list(flipped_duplicates.keys()):
        existing_u = append_existing_u(decision_tree, u_label)
        if existing_u == []:
            u = myid.generate_id()
            decision_tree.add_node(u, label=u_label)
            existing_u.append(u)
        processes_for_sub = flipped_duplicates[str(sub)]
        for process in processes_for_sub:
            for u_elem in existing_u:
                v = process
                node_id = myid.generate_id()
                decision_tree.add_node(node_id, label=v)
                decision_tree.add_edge(u_elem, node_id)

def append_existing_u(decision_tree, u_label):
    existing_u = []
    for node, data in decision_tree.nodes(data=True):
        if data['label'] == u_label:
            existing_u.append(node)
    return existing_u


# ----------------------------------------------------------------------
# PARSE TREE (PG -> block set theory, calc number of possible sequences)
# ----------------------------------------------------------------------

def build_parse_tree(assembly_tree, sorted_node_list):
    parse_tree = nx.DiGraph()
    add_nodes_with_inbetween_s_nodes(assembly_tree, parse_tree, sorted_node_list)

    #pos = hierarchy_pos(parse_tree, 's_INIT')
    #nx.draw(parse_tree, pos, with_labels=True)
    #plt.show()

    remove_unnecessary_s_nodes(parse_tree)
    remove_unnecessary_p_nodes(parse_tree)
    return parse_tree

def add_nodes_with_inbetween_s_nodes(assembly_tree, parse_tree, sorted_node_list):
    for node in sorted_node_list:
        if node == 'Start':
            add_nodes_after_serial_node(assembly_tree, parse_tree, node, 'INIT')
        else:
            parent_nodes = list(assembly_tree.predecessors(node))
            if len(parent_nodes) == 1:
                add_p_s_nodes_simple(parent_nodes, assembly_tree, node, parse_tree)
            if len(parent_nodes) > 1:
                add_p_s_nodes_with_multiple_successors(parent_nodes, assembly_tree, parse_tree, node)

        #pos = hierarchy_pos(parse_tree, 's_INIT')
        #nx.draw(parse_tree, pos, with_labels=True)
        #plt.show()

def add_p_s_nodes_simple(parent_nodes, assembly_tree, node, parse_tree):
    for parent_node in parent_nodes:
        if parent_node_is_fork(assembly_tree, node) == False:
            add_nodes_after_serial_node(assembly_tree, parse_tree, node, parent_node)
        if parent_node_is_fork(assembly_tree, node) == True:
            add_nodes_after_parallel_node(assembly_tree, parse_tree, node, parent_node)

def add_p_s_nodes_with_multiple_successors(parent_nodes, assembly_tree, parse_tree, node):
    root_node = get_local_root_of_sequence(assembly_tree, parent_nodes)

    # 2. Add this node to the "s_node" of the root
    parse_predecessor = list(parse_tree.predecessors(root_node))[0]

    # remove possible previous connections
    parse_tree.remove_edge(parse_predecessor, root_node)
    p_string_root_node = 'p_' + str(root_node)
    if parse_tree.has_edge(parse_predecessor, p_string_root_node):
        parse_tree.remove_edge(parse_predecessor, p_string_root_node)

    # add edges to additional s_node
    s_string_u = 's_' + str(root_node)
    s_string_v = 's_' + str(node)
    parse_tree.add_edge(s_string_u, root_node)
    parse_tree.add_edge(s_string_u, s_string_v)
    parse_tree.add_edge(s_string_v, node)
    parse_tree.add_edge(s_string_u, p_string_root_node)
    if parse_predecessor != s_string_u:
        parse_tree.add_edge(parse_predecessor, s_string_u)
    #if len(list(parse_tree.predecessors(node))) > 1:
    parse_tree.add_edge(s_string_v, 'p_' + str(node))

def get_local_root_of_sequence(assembly_tree, parent_nodes):
    lca = lowest_common_ancestors(assembly_tree, parent_nodes)
    #print(f'lca: {lca}')
    if len(lca) == 1:
        root_node = lca[0]
    if len(lca) == 2:
        lca_2 = lowest_common_ancestors(assembly_tree, lca)
        root_node = lca_2[0]
    else:
        root_node = 'Start' #todo: make generic for multiple lcas
    #print(f'root_node = {root_node}')
    return root_node

def lowest_common_ancestors(assembly_tree, nodes):
    # according to https://www.baeldung.com/cs/lowest-common-ancestor-acyclic-graph
    # get ancestors of each node individually
    ancestors = []
    for i, node in enumerate(nodes):
        visited = []
        ancestors_of_node = []
        dfs_get_all_ancestors(visited, ancestors_of_node, assembly_tree, node)
        ancestors.append(ancestors_of_node)

    # get common ancestors
    common_ancestors = set(ancestors[0])
    for s in ancestors[1:]:
        common_ancestors.intersection_update(s)

    # reduce graph to common ancestors
    reduced_graph = assembly_tree.copy()
    nodes_to_remove = []
    for node in assembly_tree.nodes():
        if node not in common_ancestors:
            nodes_to_remove.append(node)
    for node in nodes_to_remove:
        reduced_graph.remove_node(node)

    #nx.draw(reduced_graph, with_labels=True)
    #plt.show()

    # get leafs of reduced graph
    leafs = []
    for node in reduced_graph.nodes():
        if reduced_graph.out_degree(node) == 0:
            leafs.append(node)
    if leafs == []:
        leafs.append('Start')
    return leafs



def dfs_get_all_ancestors(visited, ancestors_of_node, graph, node):
    if node not in visited:
        visited.append(node)
        for predecessor in graph.predecessors(node):
            ancestors_of_node.append(predecessor)
            dfs_get_all_ancestors(visited, ancestors_of_node, graph, predecessor)

def get_prior_splitting_node(assembly_tree, node):
    for predecessor in assembly_tree.predecessors(node):
        if is_splitting_node(assembly_tree, predecessor):
            return predecessor
        else:
            get_prior_splitting_node(assembly_tree, predecessor)


def is_splitting_node(assembly_tree, node):
    if assembly_tree.out_degree(node) > 1:
        return True
    else:
        return False

def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)

def remove_unnecessary_s_nodes(parse_tree):
    # 1. two s_nodes after each other
    handle = []
    for node in parse_tree.nodes():
        if 's_' in str(node) and node != 's_INIT':
            parent = list(parse_tree.predecessors(node))[0]
            if 's_' in str(parent):
                ancestor_s_node = parent
                ancestor_is_s_node = True
                # 1. find root s_node
                while ancestor_is_s_node:
                    if ancestor_s_node == 's_INIT':
                        break
                    predecessor = list(parse_tree.predecessors(ancestor_s_node))[0]
                    if 's_' in predecessor:
                        ancestor_s_node = predecessor
                    else:
                        ancestor_is_s_node = False
                successors = parse_tree.successors(node)
                handle.append((node, ancestor_s_node, successors))
    for elem in handle:
        parse_tree.remove_node(elem[0])
        for successor in elem[2]:
            parse_tree.add_edge(elem[1], successor)

    # 2. s_node following after p_node, but s_node just has one outgoing node
    handle2 = []
    for node in parse_tree.nodes():
        if 's_' in str(node) and node != 's_INIT':
            if parse_tree.out_degree(node) == 1:
                parent = list(parse_tree.predecessors(node))[0]
                child = list(parse_tree.successors(node))[0]
                handle2.append((parent, node, child))
    for elem in handle2:
        parse_tree.add_edge(elem[0], elem[2])
        parse_tree.remove_node(elem[1])

def remove_unnecessary_p_nodes(parse_tree):
    # sometimes nodes p nodes are left with only one successor. Then shift the p node to the previous serial node.
    handle = []
    for node in parse_tree.nodes():
        if 'p_' in str(node):
            if parse_tree.out_degree(node) == 1:
                parent = list(parse_tree.predecessors(node))[0]
                child = list(parse_tree.successors(node))[0]
                handle.append((parent, node, child))
    for elem in handle:
        parse_tree.add_edge(elem[0], elem[2])
        parse_tree.remove_node(elem[1])


def parent_node_is_fork(assembly_tree, node):
    parent_nodes = assembly_tree.predecessors(node)
    parent_is_fork_node = False
    for p_node in parent_nodes:
        if assembly_tree.out_degree(p_node) > 1:
            parent_is_fork_node = True
            #print(f'parent node {p_node} is fork node.')
    return parent_is_fork_node

def add_nodes_after_serial_node(assembly_tree, parse_tree, node, parent_node):
    if assembly_tree.out_degree(node) == 1:
        s_string_u = 's_' + str(parent_node)
        s_string_v = 's_' + str(node)
        parse_tree.add_edge(s_string_u, s_string_v)
        parse_tree.add_edge(s_string_v, node)
    if assembly_tree.out_degree(node) > 1:
        s_string_u = 's_' + str(parent_node)
        parse_tree.add_edge(s_string_u, node)
        p_string_v = 'p_' + str(node)
        parse_tree.add_edge(s_string_u, p_string_v)
    if assembly_tree.out_degree(node) == 0:
        s_string_u = 's_' + str(parent_node)
        parse_tree.add_edge(s_string_u, node)

def add_nodes_after_parallel_node(assembly_tree, parse_tree, node, parent_node):
    if assembly_tree.out_degree(node) == 1:
        p_string_u = 'p_' + str(parent_node)
        s_string_v = 's_' + str(node)
        parse_tree.add_edge(p_string_u, s_string_v)
        parse_tree.add_edge(s_string_v, node)
    if assembly_tree.out_degree(node) > 1:
        p_string_u = 'p_' + str(parent_node)
        s_string = 's_' + str(node)
        parse_tree.add_edge(p_string_u, s_string)
        parse_tree.add_edge(s_string, node)
        p_string_v = 'p_' + str(node)
        parse_tree.add_edge(s_string, p_string_v)
    if assembly_tree.out_degree(node) == 0:
        p_string_u = 'p_' + str(parent_node)
        parse_tree.add_edge(p_string_u, node)