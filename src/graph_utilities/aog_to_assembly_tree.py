import networkx as nx
import pickle
import random
from matplotlib import pyplot as plt
from src.validation.evaluation_calculation import calc_number_of_possible_sequences

'''
graph = {1: ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 6], [3]),
         2: ([1, 2, 4, 5, 6], [1, 2, 5, 6], [4]),
         3: ([1, 2, 4, 5, 6], [1, 4, 5], [2, 6]),
         4: ([1, 2, 5, 6], [1, 5], [2, 6]),
         5: ([1, 4, 5], [1, 5], [4]),
         6: ([1, 4, 5], [1, 4], [5]),
         7: ([1, 4], [1], [4]),
         8: ([1, 5], [1], [5]),
         9: ([2, 6], [2], [6])}
'''

ioc_demo_graph = {
    1: (['be001', 'be002', 'be003', 'be004', 'be005', 'be006'], ['be001', 'be002', 'be003', 'be006'], ['be004', 'be005']),
    2: (['be001', 'be002', 'be003', 'be004', 'be005', 'be006'], ['be001', 'be002', 'be003', 'be004', 'be005'], ['be006']),
    3: (['be001', 'be002', 'be003', 'be006'], ['be001', 'be002', 'be003'], ['be006']),
    4: (['be001', 'be002', 'be003', 'be004', 'be005'], ['be001', 'be002', 'be003'], ['be004', 'be005']),
    5: (['be001', 'be002', 'be003'], ['be001', 'be003'], ['be002']),
    6: (['be001', 'be002', 'be003'], ['be001', 'be002'], ['be003']),
    7: (['be004', 'be005'], ['be004'], ['be005']),
    8: (['be001', 'be003'], ['be001'], ['be003']),
    9: (['be001', 'be002'], ['be001'], ['be002'])
}

def open_aog_file(product_name):
    pickle_path = '../../data/and_or_pickles/' + product_name + '.pickle'
    with open(pickle_path, 'rb') as input_file:
        and_or_graph = pickle.load(input_file)
    return and_or_graph

def get_assembly_tree_first_possible(graph):
    flipped_dict = get_reduced_flipped_graph(graph)
    reduced_graph = get_reduced_graph(flipped_dict, graph)

    G = nx.DiGraph()
    add_process_nodes(G, reduced_graph)
    add_edges_to_graph(G, reduced_graph, flipped_dict)
    remove_unconnected_nodes(G)
    remove_duplicate_processes(G, reduced_graph)
    add_start_node_with_edges(G)
    return G

def get_monte_carlo_optimal_assembly_tree(graph, iterations, opt_goal='high_flex'):
    #print(f'find assembly_tree with highest flexibility ({iterations} iterations)')
    best_assembly_tree = get_assembly_tree_random(graph)
    best_flex = calc_number_of_possible_sequences(best_assembly_tree)
    for i in range(0, iterations):
        assembly_tree = get_assembly_tree_random(graph).copy()
        try:
            flex = calc_number_of_possible_sequences(assembly_tree)
        except:
            # todo: Bugfix this error (Find out why for these graphs the num sequences could not be calced).
            print('exception: Could not calc flex of graph')
            if opt_goal == 'high_flex':
                flex = 0
            if opt_goal == 'low_flex':
                flex = 9999999999999999
        if opt_goal == 'high_flex':
            if flex > best_flex:
                best_assembly_tree = assembly_tree.copy()
                best_flex = flex
        if opt_goal == 'low_flex':
            if flex < best_flex:
                best_assembly_tree = assembly_tree.copy()
                best_flex = flex
    #print(f'Highest number of sequences: {best_flex}')
    return best_assembly_tree



def get_assembly_tree_random(graph):
    flipped_dict = get_random_reduced_flipped_graph(graph)
    reduced_graph = get_reduced_graph(flipped_dict, graph)

    G = nx.DiGraph()
    add_process_nodes(G, reduced_graph)
    add_edges_to_graph(G, reduced_graph, flipped_dict)
    remove_unconnected_nodes(G)
    remove_duplicate_processes(G, reduced_graph)
    add_start_node_with_edges(G)
    G.remove_edge('Start', 'Start')
    return G

def get_reduced_flipped_graph(graph):
    flipped_dict = {}
    for key, value in graph.items():
        assembly = value[0]
        assembly = str(assembly)
        if assembly not in flipped_dict:
            flipped_dict[assembly] = key
    return flipped_dict

def get_random_reduced_flipped_graph(graph):
    duplicate_keys = find_duplicate_keys(graph)
    picked_keys = pick_random_key(duplicate_keys)
    flipped_dict = {}
    for key, value in graph.items():
        assembly = str(value[0])
        if assembly not in flipped_dict:
            if assembly in picked_keys:
                picked_key = picked_keys[assembly]
                flipped_dict[assembly] = picked_key
            else:
                flipped_dict[assembly] = key
    return flipped_dict

def find_duplicate_keys(graph):
    rev_multidict = {}
    for key, value in graph.items():
        rev_multidict.setdefault(str(value[0]), set()).add(key)
    duplicate_values = [key for key, values in rev_multidict.items() if len(values) > 1]

    duplicate_keys = {}
    for key, value in graph.items():
        if str(value[0]) in duplicate_values:
            duplicate_keys.setdefault(str(value[0]), set()).add(key)
    return duplicate_keys

def pick_random_key(duplicate_keys):
    picked_keys = {}
    for key, value in duplicate_keys.items():
        picked_value = random.choice(list(value))
        picked_keys[key] = picked_value
    return picked_keys

def get_reduced_graph(flipped_dict, graph):
    reduced_graph = {}
    for key, value in flipped_dict.items():
        reduced_graph[value] = graph[value]
    return reduced_graph

def add_process_nodes(G, reduced_graph):
    for key, value in reduced_graph.items():
        G.add_node(key, assembly=value[0], sub_1=value[1], sub_2=value[2])

def add_edges_to_graph(G, reduced_graph, flipped_dict):
    for key, value in reduced_graph.items():
        target_node = key
        part_1 = value[1]
        part_2 = value[2]
        if len(part_1) > 1:
            process_for_part_1 = flipped_dict[str(part_1)]
            start_node = process_for_part_1
            G.add_edge(start_node, target_node)
        if len(part_2) > 1:
            process_for_part_2 = flipped_dict[str(part_2)]
            start_node = process_for_part_2
            G.add_edge(start_node, target_node)

def remove_unconnected_nodes(G):
    G.remove_nodes_from(list(nx.isolates(G)))

# remove duplicate processes
def remove_duplicate_processes(G, reduced_graph):
    final_process = get_final_process(reduced_graph)
    nodes_to_remove = search_nodes_to_remove(G, final_process)
    while nodes_to_remove != []:
        for node in nodes_to_remove:
            G.remove_node(node)
        nodes_to_remove = search_nodes_to_remove(G, final_process)

def search_nodes_to_remove(G, final_process):
    nodes_to_remove = []
    for node in G.nodes():
        if G.out_degree(node) == 0:
            if node != final_process:
                nodes_to_remove.append(node)
    return nodes_to_remove

def get_final_process(dict):
    key = max(dict, key=lambda k: len(dict[k]))
    return key

def add_start_node_with_edges(G):
    G.add_node('Start')
    for node in G.nodes():
        if G.in_degree(node) == 0:
            G.add_edge('Start', node)


def main():
    product_name = 'ioc_demo'
    and_or_graph = open_aog_file(product_name)



    G = get_monte_carlo_optimal_assembly_tree(and_or_graph, iterations=100, opt_goal='high_flex')

    nx.draw(G, with_labels=True)
    plt.show()

    pickle_path = '../../out/assembly_tree/' + product_name + '.pickle'
    with open(pickle_path, 'wb') as handle:
        pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    main()
