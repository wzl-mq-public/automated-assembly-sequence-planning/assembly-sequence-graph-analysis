#
#
#       DEPRECIATED: SKRIPT NOT WORKING, TOO COMPLEX --> refer to aog_to_assembly_tree.py
#
#



import pickle
import random
import networkx as nx
from matplotlib import pyplot as plt

def create_random_assembly_tree(process_graph):
    G = nx.DiGraph()
    G.add_node('S')

    decision_nodes = get_decision_nodes_with_multiple_out(process_graph)
    print(f'decision_nodes: {decision_nodes}')

    simple_nodes = [node for node in process_graph.nodes() if (node not in decision_nodes) and (('S' or 's') not in node)]
    print(f'simple_nodes: {simple_nodes}')

    for u,v,a in process_graph.edges(data=True):
        print(u, v, a)

        if a['edge_type'] == 'real':
            G.add_node(a['label'], assembly_parts=a['assembly_parts'])


        if a['source_node_type'] == 'start':
            G.add_edge('S', a['label'])


        print(f'u_node = {u}')
        print(f'v_node = {v}')
        u_process = a['label']
        print(f'u_process = {u_process}')

        successors = list(process_graph.successors(v))
        print(f'successors = {successors}')
        if successors != []:
            if 'r' in v:
                picked_successor = random.choice(successors)
                print(f'picked successor = {picked_successor}')
                v_process = process_graph[v][picked_successor]['label']
                print(f'v_process = {v_process}')
                if u_process and v_process in G:
                    G.add_edge(u_process, v_process)
            if 's' in v:
                for successor in successors:
                    v_process = process_graph[v][successor]['label']
                    if u_process and v_process in G:
                        G.add_edge(u_process, v_process)
        if a['edge_type'] == 'dummy':



    return G

def create_optimal_assembly_tree(process_graph):
    #todo: use gurobi solver from Mikhail
    pass

def get_decision_nodes_with_multiple_out(process_graph):
    decision_nodes = []
    for n in process_graph.nodes():
        successors = list(process_graph.successors(n))
        if 'r' in n:
            if len(successors) > 1:
                decision_nodes.append(n)
    return decision_nodes



def main():
    # Load process graph
    product_name = 'simple_example'
    pickle_path = '../../out/process_graph/' + product_name + '.pickle'
    with open(pickle_path, 'rb') as input_file:
        process_graph = pickle.load(input_file)

    assembly_tree = create_random_assembly_tree(process_graph)
    nx.draw(assembly_tree, with_labels=True)
    plt.show()

if __name__ == '__main__':
    main()