import pickle

product_name = 'centrifugal_pump'
pickle_path = '../../data/and_or_pickles/' + product_name + '.pickle'
with open(pickle_path, 'rb') as input_file:
    and_or_graph = pickle.load(input_file)

print(and_or_graph)
translation_dict = {
    1: 0,
    2: 1,
    3: 2,
    4: 3,
    5: 5,
    6: 6,
    7: 7,
    8: 18,
    9: 19,
    10: 20,
    11: 21,
    12: 22,
}

def translate_array(array, translation_dict):
    t_array = []
    for elem in array:
        t_array.append(translation_dict[elem])
    return t_array

translated_graph = {}
for key, value in and_or_graph.items():
    t_assembly = translate_array(value[0], translation_dict)
    t_sub_1 = translate_array(value[1], translation_dict)
    t_sub_2 = translate_array(value[2], translation_dict)
    t_value = (t_assembly, t_sub_1, t_sub_2)
    translated_graph[key] = t_value

print(translated_graph)

pickle_path = '../../data/and_or_pickles/' + product_name + '_translated.pickle'
with open(pickle_path, 'wb') as handle:
    pickle.dump(translated_graph, handle, protocol=pickle.HIGHEST_PROTOCOL)