# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 15:11:03 2020

@author: mikep
"""

import networkx as nx
import random as rnd
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import time

'''
Receives a dictionary {int: tuple of lists} that contains AND/OR hypergraph and various process data
'''


def generate_process_graph_dict(and_or_graph):
    start_process_graph = time.time()

    # ------------------------------------------------------------
    # Initialize decision and splitting nodes of the process graph
    # ------------------------------------------------------------

    assembly_tasks = and_or_graph.keys()

    decision_nodes = []
    ingoing_tasks_of_decision_node_r = {}
    outgoing_tasks_of_decision_node_r = {}

    splitting_nodes = []
    s_ind = 1
    ingoing_tasks_of_splitting_node_s = {}
    outgoing_tasks_of_splitting_node_s = {}

    dummy_operation_index = len(assembly_tasks) + 1

    explored_parent_subassemblies = []
    # If an r-node has multiple outgoing tasks, they have to be replaced by dummy operations
    # For k that create such r-nodes, the replacement can be encoded as a dictionary of dictionaries
    # {k: {operation to be replaced: dummy operation}}
    task_replacement = {}

    # -----------------------------------------
    # Pupulate variables for given and_or_graph
    # -----------------------------------------

    for assembly_task in assembly_tasks:
        # Create a decision node in the process graph

        # Product subassembly of operation k
        h = get_product_subassembly(assembly_task, and_or_graph)

        if h not in explored_parent_subassemblies:
            decision_nodes.append(assembly_task)

        tasks_in = []
        tasks_out = []

        for k_ in assembly_tasks:

            # If h is a component in task k_,
            # such task is an outgoing task
            if subassembly_is_component_in_task(h, k_, and_or_graph):
                tasks_out.append(k_)

            # If the product of a task k_ is h,
            # such task is an ingoing task
            if subassembly_is_product_in_task(h, k_, and_or_graph):
                tasks_in.append(k_)

                # If both components of the ingoing task have at least one task
                # which assembles them, insert a splitting node
                h_1 = and_or_graph[k_][1]
                h_2 = and_or_graph[k_][2]

                if has_assembly_operation(h_1, and_or_graph) and has_assembly_operation(h_2, and_or_graph) and assembly_task == k_:

                    splitting_nodes.append(s_ind)
                    s_tasks_in = []
                    s_tasks_out = []

                    h_1_in_ops = count_ingoing_assembly_ops(h_1, and_or_graph)
                    h_2_in_ops = count_ingoing_assembly_ops(h_2, and_or_graph)

                    h_1_out_ops = count_outgoing_assembly_ops(h_1, and_or_graph)
                    h_2_out_ops = count_outgoing_assembly_ops(h_2, and_or_graph)

                    for k_io in assembly_tasks:
                        # If the product of task k_io is one of the components in task k_
                        # such task is an ingoing task of the splitting node
                        # An ingoing operation is only added if it is the only way to assemble h_1 or h_2
                        # and the components of such operation are only involved in one assembly operation
                        if subassembly_is_product_in_task(h_1, k_io, and_or_graph) and h_1_in_ops == 1 and h_1_out_ops == 1:
                            s_tasks_in.append(k_io)
                        if subassembly_is_product_in_task(h_2, k_io, and_or_graph) and h_2_in_ops == 1 and h_2_out_ops == 1:
                            s_tasks_in.append(k_io)

                    s_tasks_out.append(k_)

                    # Insert a dummy operation between r and s if there are more than 1 in/outgoing tasks
                    replacements = {}

                    if h_1_in_ops > 1 or h_1_out_ops > 1:
                        fiat = first_ingoing_assembly_task(h_1, assembly_tasks, and_or_graph)
                        if h_1_out_ops > 1 and h_1_in_ops > 1:
                            pass
                        else:
                            for out_task in outgoing_assembly_tasks(h_1, assembly_tasks, and_or_graph):
                                if out_task == k_:
                                    replacements[out_task] = dummy_operation_index
                            if fiat in task_replacement:
                                task_replacement[fiat].update(dict(replacements))
                            else:
                                task_replacement[fiat] = dict(replacements)
                        s_tasks_in.append(dummy_operation_index)
                        dummy_operation_index = dummy_operation_index + 1

                    if h_2_in_ops > 1 or h_2_out_ops > 1:
                        fiat = first_ingoing_assembly_task(h_2, assembly_tasks, and_or_graph)
                        if h_2_out_ops > 1 and h_2_in_ops > 1:
                            pass
                        else:
                            for out_task in outgoing_assembly_tasks(h_2, assembly_tasks, and_or_graph):
                                if out_task == k_:
                                    replacements[out_task] = dummy_operation_index
                            if fiat in task_replacement:
                                task_replacement[fiat].update(dict(replacements))
                            else:
                                task_replacement[fiat] = dict(replacements)
                        s_tasks_in.append(dummy_operation_index)
                        dummy_operation_index = dummy_operation_index + 1


                    ingoing_tasks_of_splitting_node_s[s_ind] = s_tasks_in
                    outgoing_tasks_of_splitting_node_s[s_ind] = s_tasks_out
                    s_ind = s_ind + 1

        if h not in explored_parent_subassemblies:
            # Remember this parent subassembly as explored
            explored_parent_subassemblies.append(h)
            # Connect decision node
            ingoing_tasks_of_decision_node_r[assembly_task] = tasks_in
            outgoing_tasks_of_decision_node_r[assembly_task] = tasks_out

    # Replace operations with dummies where needed
    for k_in in ingoing_tasks_of_decision_node_r:
        first_in_task = ingoing_tasks_of_decision_node_r[k_in][0]
        if first_in_task in task_replacement:
            for i in range(len(outgoing_tasks_of_decision_node_r[k_in])):
                k_out = outgoing_tasks_of_decision_node_r[k_in][i]
                if k_out in task_replacement[first_in_task]:
                    outgoing_tasks_of_decision_node_r[k_in][i] = task_replacement[first_in_task][k_out]

    # Remove redundant decision nodes (logical copies of splitting nodes)
    for s in outgoing_tasks_of_splitting_node_s.keys():
        for r in outgoing_tasks_of_decision_node_r.copy().keys():
            if outgoing_tasks_of_splitting_node_s[s] == outgoing_tasks_of_decision_node_r[r]:
                del outgoing_tasks_of_decision_node_r[r]
                del ingoing_tasks_of_decision_node_r[r]

    print("Process graph generated_screening.")
    end_process_graph = time.time()
    print("Process graph generation took: " + str(end_process_graph - start_process_graph))
    print("Number of decision nodes:  " + str(len(decision_nodes)))
    print("Number of splitting nodes: " + str(len(splitting_nodes)))
    print("Number of tasks: " + str(dummy_operation_index))

    # -----------------------------------------------------------------
    # Dictionary of dictionaries to prepare process graph visualisation
    # -----------------------------------------------------------------

    process_graph = {}
    # Iterate over decision nodes' outgoing tasks
    for r_out in outgoing_tasks_of_decision_node_r.keys():
        for k_out in outgoing_tasks_of_decision_node_r[r_out]:
            # Iterate over decision nodes' ingoing tasks
            make_dict_of_dicts(r_out, k_out, ingoing_tasks_of_decision_node_r, 'r', 'r', process_graph, and_or_graph)
            # Iterate over splitting nodes' ingoing tasks
            make_dict_of_dicts(r_out, k_out, ingoing_tasks_of_splitting_node_s, 'r', 's', process_graph, and_or_graph)

    # Iterate over splitting nodes' outgoing tasks
    for s_out in outgoing_tasks_of_splitting_node_s.keys():
        for k_out in outgoing_tasks_of_splitting_node_s[s_out]:
            # Iterate over decision nodes' ingoing tasks
            make_dict_of_dicts(s_out, k_out, ingoing_tasks_of_decision_node_r, 's', 'r', process_graph, and_or_graph)
            # Iterate over splitting nodes' ingoing tasks
            make_dict_of_dicts(s_out, k_out, ingoing_tasks_of_splitting_node_s, 's', 's', process_graph, and_or_graph)

    # Check for ingoing tasks without source nodes and connect them to a "start" node
    for r_in in ingoing_tasks_of_decision_node_r.keys():
        for k_in in ingoing_tasks_of_decision_node_r[r_in]:
            has_source_node = False
            for out_tasks in list(outgoing_tasks_of_decision_node_r.values()) + list(outgoing_tasks_of_splitting_node_s.values()):
                if k_in in out_tasks:
                    has_source_node = True
                    break
            if has_source_node == False:
                target_node = 'r' + str(r_in)
                edge_type = ''
                if k_in <= max(and_or_graph.keys()):
                    edge_type = 'real'
                    assembly_parts = and_or_graph[k_in]
                else:
                    edge_type = 'dummy'
                    assembly_parts = []
                source_label = 'S'
                if source_label in process_graph.keys():
                    process_graph[source_label].update({target_node: {'label': str(k_in),
                                                                     'edge_type': edge_type,
                                                                     'source_node_type': 'start',
                                                                     'target_node_type': 'r',
                                                                     'assembly_parts': assembly_parts}})
                else:
                    process_graph[source_label] = {target_node: {'label': str(k_in),
                                                                     'edge_type': edge_type,
                                                                     'source_node_type': 'start',
                                                                     'target_node_type': 'r',
                                                                     'assembly_parts': assembly_parts}}

    for s_in in ingoing_tasks_of_splitting_node_s.keys():
        for k_in in ingoing_tasks_of_splitting_node_s[s_in]:
            has_source_node = False
            for out_tasks in list(outgoing_tasks_of_decision_node_r.values()) + list(outgoing_tasks_of_splitting_node_s.values()):
                if k_in in out_tasks:
                    has_source_node = True
                    break
            if has_source_node == False:
                target_node = 's' + str(s_in)
                edge_type = ''
                if k_in <= max(and_or_graph.keys()):
                    edge_type = 'real'
                    assembly_parts = and_or_graph[k_out]
                else:
                    edge_type = 'dummy'
                    assembly_parts = []
                source_label = 'S'
                if source_label in process_graph.keys():
                    process_graph[source_label].update({target_node: {'label': str(k_in),
                                                                     'edge_type': edge_type,
                                                                     'source_node_type': 'start',
                                                                     'target_node_type': 's',
                                                                      'assembly_parts': assembly_parts}})
                else:
                    process_graph[source_label] = {target_node: {'label': str(k_in),
                                                                     'edge_type': edge_type,
                                                                     'source_node_type': 'start',
                                                                     'target_node_type': 's',
                                                                 'assembly_parts': assembly_parts}}


    # Redefine O_j as containing dummy operations
    assembly_tasks = range(1, dummy_operation_index)

    # Set of task pairs (k, l) with k --> l
    # Define P_j with the complete process graph (including dummy activities)

    P_j = []
    for r_in in ingoing_tasks_of_decision_node_r.keys():
        for k_in in ingoing_tasks_of_decision_node_r[r_in]:
            for k_out in outgoing_tasks_of_decision_node_r[r_in]:
                pair = (k_in, k_out)
                P_j.append(pair)
    for s_in in ingoing_tasks_of_splitting_node_s.keys():
        for k_in in ingoing_tasks_of_splitting_node_s[s_in]:
            for k_out in outgoing_tasks_of_splitting_node_s[s_in]:
                pair = (k_in, k_out)
                if pair not in P_j:
                    P_j.append(pair)

    print("Dict of dicts representation ready.")
    return process_graph

# Find direct and indirect predecessors of an operation recursively
def find_predecessors(k, P_j, pred):
    pred_temp = []
    for pair in P_j:
        if pair[1] == k:
            pred.append(pair[0])
            pred_temp.append(pair[0])
    for m in pred_temp:
        find_predecessors(m, P_j, pred)

# Determines whether there are assembly operations that create such subassembly
def has_assembly_operation(subassy, and_or_graph):
    for op in and_or_graph.values():
        if op[0] == subassy:
            return True
    return False

# Counts assembly operations that create this subassembly
def count_ingoing_assembly_ops(subassy, and_or_graph):
    count = 0
    for op in and_or_graph.values():
        if op[0] == subassy:
            count = count + 1
    return count

# Counts assembly operations that use this subassembly
def count_outgoing_assembly_ops(subassy, and_or_graph):
    count = 0
    for op in and_or_graph.values():
        if op[1] == subassy or op[2] == subassy:
            count = count + 1
    return count

def get_product_subassembly(k, and_or_graph):
    return and_or_graph[k][0]

def subassembly_is_component_in_task(h, k_, and_or_graph):
    if h in and_or_graph[k_] and h != and_or_graph[k_][0]:
        return True
    else:
        return False

def subassembly_is_product_in_task(h, k_, and_or_graph):
    if get_product_subassembly(k_, and_or_graph) == h:
        return True
    else:
        return False

def first_ingoing_assembly_task(h, O_j, and_or_graph):
    for k in O_j:
        if subassembly_is_product_in_task(h, k, and_or_graph):
            return k
    return -1

def outgoing_assembly_tasks(h, O_j, and_or_graph):
    out_tasks = []
    for k in O_j:
        if and_or_graph[k][1] == h or and_or_graph[k][2] == h:
            out_tasks.append(k)
    return out_tasks

def make_dict_of_dicts(source_index, k_out, ingoing_tasks, source_type, target_type, process_graph, and_or_graph):
    # Iterate over nodes' ingoing tasks
    for i in ingoing_tasks.keys():
        for k_in in ingoing_tasks[i]:
            if k_out == k_in:
                target_node = target_type + str(i)
                edge_type = ''
                if k_out <= max(and_or_graph.keys()):
                    edge_type = 'real'
                    assembly_parts = and_or_graph[k_out]
                else:
                    edge_type = 'dummy'
                    assembly_parts = []
                source_label = source_type + str(source_index)
                if source_label in process_graph.keys():
                    process_graph[source_label].update({target_node: {'label': str(k_out),
                                                                      'edge_type': edge_type,
                                                                      'source_node_type': source_type,
                                                                      'target_node_type': target_type,
                                                                      'assembly_parts': assembly_parts}},)
                else:
                    process_graph[source_label] = {target_node: {'label': str(k_out),
                                                                 'edge_type': edge_type,
                                                                 'source_node_type': source_type,
                                                                 'target_node_type': target_type,
                                                                 'assembly_parts': assembly_parts}}


def generate_nx_process_graph(process_graph):
    fig3 = plt.figure()
    fig3.set_size_inches(15, 15)

    G = nx.DiGraph(process_graph)
    pos = nx.layout.circular_layout(G)

    for source in process_graph.keys():
        for target in process_graph[source].keys():
            edge = (source, target)
            label = process_graph[source][target]['label']
            source_node_color = ''
            if process_graph[source][target]['source_node_type'] == 'r':
                source_node_color = '#000000'
            elif process_graph[source][target]['source_node_type'] == 's':
                source_node_color = '#ffffff'
            elif process_graph[source][target]['source_node_type'] == 'start':
                source_node_color = '#ffffff'
            target_node_color = ''
            if process_graph[source][target]['target_node_type'] == 'r':
                target_node_color = '#000000'
            elif process_graph[source][target]['target_node_type'] == 's':
                target_node_color = '#ffffff'
            # Draw source nodes (incl. start node with label 'S')
            if process_graph[source][target]['source_node_type'] == 'start':
                nx.draw_networkx_labels(G, pos=pos, labels={'S': 'S'})
            nx.draw_networkx_nodes(G, pos=pos, nodelist=[source], node_color=source_node_color,
                                   edgecolors=['#000000'])
            # Draw target nodes
            nx.draw_networkx_nodes(G, pos=pos, nodelist=[target], node_color=target_node_color,
                                   edgecolors=['#000000'])
            if process_graph[source][target]['edge_type'] == 'real':
                nx.draw_networkx_edges(G, pos=pos, edgelist=[edge], style='solid')
            elif process_graph[source][target]['edge_type'] == 'dummy':
                collection = nx.draw_networkx_edges(G, pos=pos, edgelist=[edge])
                for patch in collection:
                    patch.set_linestyle('dashed')
            nx.draw_networkx_edge_labels(G, pos=pos, edge_labels={edge: label})

    plt.show()
    return G

if __name__ == '__main__':
    '''
    # Open serialized AND/OR graph
    product_name = 'centrifugal_pump'
    with open('../../data/and_or_pickles/' + product_name + '.pickle', 'rb') as f:
        graph = pickle.load(f)

    '''
    # Product 1
    product_name = 'simple_example'
    graph = {1: ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 6], [3]),
             2: ([1, 2, 4, 5, 6], [1, 2, 5, 6], [4]),
             3: ([1, 2, 4, 5, 6], [1, 4, 5], [2, 6]),
             4: ([1, 2, 5, 6], [1, 5], [2, 6]),
             5: ([1, 4, 5], [1, 5], [4]),
             6: ([1, 4, 5], [1, 4], [5]),
             7: ([1, 4], [1], [4]),
             8: ([1, 5], [1], [5]),
             9: ([2, 6], [2], [6])}



    process_graph_dict = generate_process_graph_dict(graph)
    G = generate_nx_process_graph(process_graph_dict)


    pickle_path = '../../out/process_graph/' + product_name + '.pickle'
    with open(pickle_path, 'wb') as handle:
        pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)


    print('Done')
