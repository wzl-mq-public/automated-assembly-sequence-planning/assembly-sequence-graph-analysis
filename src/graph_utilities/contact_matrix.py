import numpy as np
import pandas as pd
import itertools

from src.abd_by_occ.basic_cad_functions.part_handling import remove_fasteners, rename_shapes_with_numbers
from OCC.Core.GProp import GProp_GProps
from OCC.Extend.DataExchange import read_step_file_with_names_colors
from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.BRepExtrema import BRepExtrema_ShapeProximity,BRepExtrema_ShapeList,BRepExtrema_TriangleSet
from OCC.Extend.ShapeFactory import translate_shp
from OCC.Core.gp import gp_Pnt, gp_Vec

props = GProp_GProps()

product_name = 'centrifugal_pump'
FILEPATH = '../../examples/stepFiles/' + product_name + '.stp'


def transpose_add(df_array):
    df_array = df_array + df_array.T - np.diag(np.diag(df_array))
    return df_array

def proximity(shapes_object,make_csv=False, export_name= product_name + '.csv'):
    names = []
    for shape in shapes_object:
        label, _ = shapes_object[shape]
        names.append(label)

    contacts = np.zeros(shape=[len(names),len(names)])
    print("Making array of shape:",contacts.shape)

    for i, j in itertools.combinations(np.arange(len(list(shapes_object.keys()))), 2):
        if i>j:
            print(i,j)
        shape_a = list(shapes_object.keys())[i]
        shape_b = list(shapes_object.keys())[j]
        aMesher = BRepMesh_IncrementalMesh(shape_a, 1.0)
        aMesher = BRepMesh_IncrementalMesh(shape_b, 1.0)
        proximity = BRepExtrema_ShapeProximity(shape_a, shape_b)
        proximity.Perform()
        shapes_in_contact_a = []
        shapes_in_contact_b = []
        if proximity.IsDone():
            subs1 = proximity.OverlapSubShapes1().Keys()
            subs2 = proximity.OverlapSubShapes2().Keys()

            for sa in subs1:
                temp = translate_shp(proximity.GetSubShape1(sa), gp_Vec(0, 0, 5))
                shapes_in_contact_a.append(temp)
            for sa in subs2:
                temp = translate_shp(proximity.GetSubShape2(sa), gp_Vec(0, 0, 5))
                shapes_in_contact_b.append(temp)
            if (len(shapes_in_contact_a) and len(shapes_in_contact_b)):
                if j>i:
                    contacts[i,j] = 1
                else:
                    print(i,j)

    if make_csv:
        print("Preparing dataframe")
        contacts = transpose_add(contacts)
        df_contact = pd.DataFrame(contacts,index=names,columns=names)
        df_contact.to_csv('../../out/contact_matrix/' + export_name)

    return contacts


if __name__ == "__main__":
    shapes_names_colors = read_step_file_with_names_colors(FILEPATH)
    shapes_names_colors = rename_shapes_with_numbers(shapes_names_colors)
    shapes_names_colors = remove_fasteners(shapes_names_colors)
    proximity(shapes_names_colors,make_csv=True)
    
