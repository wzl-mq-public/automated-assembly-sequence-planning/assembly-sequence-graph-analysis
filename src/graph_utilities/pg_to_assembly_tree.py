from typing import Dict, Any

import networkx as nx
import pickle
import random
from matplotlib import pyplot as plt

def open_pg_file(product_name):
    pickle_path = '../../out/precedence_graph/' + product_name + '.pickle'
    with open(pickle_path, 'rb') as input_file:
        pg = pickle.load(input_file)
    return pg

def get_initial_part(G):
    initial_part = [n for n,d in G.in_degree() if d==0]
    initial_part = initial_part[0]
    return initial_part

def get_all_initial_parts(G):
    initial_parts = [n for n, d in G.in_degree() if d == 0]
    return initial_parts

def init_mapped_data_with_sub_2(G):
    # get added parts (sub_2)
    mapped_data = {}
    process_id = 1
    for node, data in G.nodes(data=True):
        if data['layer'] != 0:
            mapped_data[process_id] = {'sub_2': [node]}
            process_id += 1
    return mapped_data

def relabel_all_nodes(G):
    # relabel all nodes with process index:
    mapping = {old_label:new_label for new_label, old_label in enumerate(G.nodes())}
    print('Switching names of nodes with:')
    print(mapping)
    G = nx.relabel_nodes(G, mapping)
    mapping = {0:'Start'}
    G = nx.relabel_nodes(G, mapping)
    return G

def relabel_all_nodes_ignoring_start(G):
    # relabel all nodes with process index:
    mapping = {old_label:new_label for new_label, old_label in enumerate(G.nodes()) if old_label!='Start'}
    print('Switching names of nodes with:')
    print(mapping)
    G = nx.relabel_nodes(G, mapping)
    return G

def get_first_tier_node_info(G, mapped_data, initial_part):
    for node, data in G.nodes(data=True):
        if data['layer'] == 1:
            sub_1 = [initial_part]
            mapped_data[node]['sub_1'] = sub_1
            sub_2 = mapped_data[node]['sub_2']
            assembly = sub_1 + sub_2
            mapped_data[node]['assembly'] = assembly
    return mapped_data


def get_node_info_for_other_tiers(G, mapped_data):
    for node, data in G.nodes(data=True):
        if data['layer'] > 1:
            print(f'node: {node, data}')
            predecessors = [n for n in G.predecessors(node)]

            # get parts from predecessors
            sub_1 = []
            for predecessor in predecessors:
                print(f'predecessor: {predecessor}, mapped_data: {mapped_data[predecessor]}')
                parts_to_add = mapped_data[predecessor]['assembly']
                for part in parts_to_add:
                    if part not in sub_1:
                        sub_1.append(part)

            mapped_data[node]['sub_1'] = sub_1
            sub_2 = mapped_data[node]['sub_2']
            assembly = sub_1 + sub_2
            mapped_data[node]['assembly'] = assembly
    return mapped_data

def add_mapped_data_to_nodes(G, mapped_data):
    for key, value in mapped_data.items():
        assembly = value['assembly']
        sub_1 = value['sub_1']
        sub_2 = value['sub_2']
        G.nodes[key]['assembly'] = assembly
        G.nodes[key]['sub_1'] = sub_1
        G.nodes[key]['sub_2'] = sub_2
    return G

def main():
    product_name = 'Product2_from_Catia'
    G = open_pg_file(product_name)

    # Verifikation
    for node, data in G.nodes(data=True):
        print(node, data)

    initial_part = get_initial_part(G)

    mapped_data = init_mapped_data_with_sub_2(G)
    G = relabel_all_nodes(G)
    mapped_data = get_first_tier_node_info(G, mapped_data, initial_part)
    mapped_data = get_node_info_for_other_tiers(G, mapped_data)
    G = add_mapped_data_to_nodes(G, mapped_data)

    # Verifikation
    for node, data in G.nodes(data=True):
        print(node, data)

    pos = nx.multipartite_layout(G, subset_key='layer')
    plt.figure(figsize=(8, 8))
    nx.draw(G, pos, with_labels=True)
    plt.axis("equal")
    plt.show()

    pickle_path = '../../out/assembly_tree/' + product_name + '_from_pg.pickle'
    with open(pickle_path, 'wb') as handle:
        pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    main()