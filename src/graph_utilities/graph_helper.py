import networkx as nx
import matplotlib.pyplot as plt
import random
import operator
import pandas as pd
from itertools import chain

class ID_Generator:
    def __init__(self):
        self.id = 0

    def generate_id(self):
        self.id = self.id + 1
        return self.id

def sort_by_values_len(dict):
    dict_len = {key: len(value[0]) for key, value in dict.items()}
    sorted_key_list = sorted(dict_len.items(), key=operator.itemgetter(1), reverse=True)
    sorted_list = [{item[0]: dict[item [0]]} for item in sorted_key_list]
    sorted_dict = {}
    for i, entry in enumerate(sorted_list):
        sorted_dict[i] = list(entry.values())[0]
    return sorted_dict

def plot_decision_tree(decision_tree, color_map=[]):
    labeldict = {}
    for node, data in decision_tree.nodes(data=True):
        try:
            labeldict[node] = data['label']
        except:
            print('key error label')
    try:
        pos = hierarchy_pos(decision_tree)
    except:
        pos = nx.circular_layout(decision_tree)
    nx.draw(decision_tree, pos, labels=labeldict, node_color=color_map, with_labels=True)
    plt.show()

def get_color_map(decision_tree, simple_nodes):
    color_map = []
    for node, data in decision_tree.nodes(data=True):
        if data['label'] in simple_nodes:
            color_map.append('blue')
        else:
            color_map.append('red')
    return color_map

def hierarchy_pos(G, root=None, width=1., vert_gap=0.2, vert_loc=0, xcenter=0.5):
    '''
    From Joel's answer at https://stackoverflow.com/a/29597209/2966723.
    Licensed under Creative Commons Attribution-Share Alike

    If the graph is a tree this will return the positions to plot this in a
    hierarchical layout.

    G: the graph (must be a tree)

    root: the root node of current branch
    - if the tree is directed and this is not given,
      the root will be found and used
    - if the tree is directed and this is given, then
      the positions will be just for the descendants of this node.
    - if the tree is undirected and not given,
      then a random choice will be used.

    width: horizontal space allocated for this branch - avoids overlap with other branches

    vert_gap: gap between levels of hierarchy

    vert_loc: vertical location of root

    xcenter: horizontal location of root
    '''
    if not nx.is_tree(G):
        raise TypeError('cannot use hierarchy_pos on a graph that is not a tree')

    if root is None:
        if isinstance(G, nx.DiGraph):
            root = next(iter(nx.topological_sort(G)))  # allows back compatibility with nx version 1.11
        else:
            root = random.choice(list(G.nodes))

    def _hierarchy_pos(G, root, width=1., vert_gap=0.2, vert_loc=0, xcenter=0.5, pos=None, parent=None):
        '''
        see hierarchy_pos docstring for most arguments

        pos: a dict saying where all nodes go if they have been assigned
        parent: parent of this branch. - only affects it if non-directed

        '''

        if pos is None:
            pos = {root: (xcenter, vert_loc)}
        else:
            pos[root] = (xcenter, vert_loc)
        children = list(G.neighbors(root))
        if not isinstance(G, nx.DiGraph) and parent is not None:
            children.remove(parent)
        if len(children) != 0:
            dx = width / len(children)
            nextx = xcenter - width / 2 - dx / 2
            for child in children:
                nextx += dx
                pos = _hierarchy_pos(G, child, width=dx, vert_gap=vert_gap,
                                     vert_loc=vert_loc - vert_gap, xcenter=nextx,
                                     pos=pos, parent=root)
        return pos

    return _hierarchy_pos(G, root, width, vert_gap, vert_loc, xcenter)

def init_assembly_tiers(assembly_tree_graph, initial_node='Start'):
    tier_list = []
    tier_list.append([initial_node])
    current_tier = 0
    while True:
        next_tier_nodes = []
        next_neighbor_nodes = get_next_neighbor_nodes(assembly_tree_graph, tier_list[current_tier])
        if next_neighbor_nodes != []:
            for node in next_neighbor_nodes:
                if predecessors_already_visited(node, tier_list, assembly_tree_graph):
                    next_tier_nodes.append(node)
            tier_list.append(next_tier_nodes)
            current_tier += 1
        else:
            break
    return tier_list

def init_assembly_tiers_multiple_start_nodes(assembly_tree_graph, initial_nodes):
    tier_list = []
    tier_list.append(initial_nodes)
    current_tier = 0
    while True:
        next_tier_nodes = []
        next_neighbor_nodes = get_next_neighbor_nodes(assembly_tree_graph, tier_list[current_tier])
        if next_neighbor_nodes != []:
            for node in next_neighbor_nodes:
                if predecessors_already_visited(node, tier_list, assembly_tree_graph):
                    next_tier_nodes.append(node)
            tier_list.append(next_tier_nodes)
            current_tier += 1
        else:
            break
    return tier_list

def update_at_df_with_tier_list(at_df, tier_list):
    for i, tier in enumerate(tier_list):
        for elem in tier:
            at_df.loc[at_df.Product == elem, 'Assembly Tier'] = i
    return at_df


def predecessors_already_visited(node, tier_list, G):
    flatten_tier_list = list(chain.from_iterable(tier_list))
    for predecessor in G.predecessors(node):
        if predecessor in flatten_tier_list:
            return True
        else:
            return False

def add_tier_to_nodes(assembly_tiers, graph):
    for counter, tier in enumerate(assembly_tiers):
        for elem in tier:
            if elem in graph.nodes():
                graph.nodes[elem]['tier'] = counter

def get_next_neighbor_nodes(g, current_tier_nodes):
    next_tier_nodes = []
    for node in current_tier_nodes:
        for neighbor in g.neighbors(node):
            if neighbor not in next_tier_nodes:
                if neighbor != node:
                    next_tier_nodes.append(neighbor)
    return next_tier_nodes

def get_topological_sort_list_by_assembly_tiers(graph, initial_node='Start'):
    assembly_tiers = init_assembly_tiers(graph, initial_node)
    add_tier_to_nodes(assembly_tiers, graph)

    sorted_node_list = sorted(graph.nodes(), key=lambda n: graph.nodes[n]['tier'])
    return sorted_node_list

def get_random_sequence_from_PG(graph):
    num_nodes = len(graph.nodes()) - 1
    assembly_sequence = []

    possible_nodes = list(graph.successors('Start'))

    while len(assembly_sequence) < num_nodes:
        choice = random.choice(possible_nodes)
        assembly_sequence.append(choice)
        possible_nodes.remove(choice)
        next_successors = graph.successors(choice)
        for successor in next_successors:
            if all_predecessors_already_visited(graph, assembly_sequence, successor):
                possible_nodes.append(successor)
    return assembly_sequence

def estimate_num_sequences(atg, max_iter):
    unique_sequences = []
    iter_counter = 0
    while (iter_counter < max_iter):
        random_sequence = get_random_sequence_from_PG(atg)
        if random_sequence not in unique_sequences:
            unique_sequences.append(random_sequence)
        iter_counter += 1
    return len(unique_sequences)


def all_predecessors_already_visited(graph, visited, node):
    predecessors = graph.predecessors(node)
    num_predecessors = len(list(predecessors))
    true_counter = 0
    for n in graph.predecessors(node):
        if n in visited:
            true_counter += 1
    if true_counter == num_predecessors:
        return True
    else:
        return False



