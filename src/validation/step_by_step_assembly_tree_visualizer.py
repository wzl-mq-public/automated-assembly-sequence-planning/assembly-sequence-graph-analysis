from OCC.Extend.DataExchange import read_step_file_with_names_colors
from OCC.Display.SimpleGui import init_display
from OCC.Core.Quantity import Quantity_Color, Quantity_TOC_RGB
from src.abd_by_occ.basic_cad_functions.part_handling import remove_fasteners, rename_shapes_with_numbers
import pickle
import networkx as nx
import matplotlib.pyplot as plt
from random import shuffle
from itertools import chain

class View():
    def setup(self, controller):
        self.display, self.start_display, self.add_menu, self.add_function_to_menu = init_display()
        self.base_color = Quantity_Color(0.7, 0.7, 0.7, Quantity_TOC_RGB)
        self.current_color = Quantity_Color(0.7, 0.1, 0.1, Quantity_TOC_RGB)
        self.assembly_compound = {}
        self.current_compound = {}
        self.current_assembly_tier = 0
        self.current_assembly_sequence_index = 0
        self.add_menu('Edit')
        self.add_function_to_menu('Edit', controller.init_part)
        self.add_function_to_menu('Edit', controller.next_tier)
        self.add_function_to_menu('Edit', controller.next_part)

    def update_compounds_with_sequence(self, assembly_sequence, full_compound, assembly_tree_graph):
        self.assembly_compound = {}
        self.current_compound = {}
        node = assembly_sequence[self.current_assembly_sequence_index]
        sub_1 = assembly_tree_graph.nodes[node]['sub_1']
        sub_2 = assembly_tree_graph.nodes[node]['sub_2']

        for part in sub_1:
            self.assembly_compound = append_shape_to_compound(full_compound, self.assembly_compound, part)
        for part in sub_2:
            self.current_compound = append_shape_to_compound(full_compound, self.current_compound, part)

    def update_compounds_with_tiers(self, assembly_tiers, full_compound):
        self.assembly_compound.update(self.current_compound)
        self.current_compound = {}
        for node in assembly_tiers[self.current_assembly_tier]:
            self.current_compound = append_shape_to_compound(full_compound, self.current_compound, node)

    def display_assembly_compound(self):
        for shape in self.assembly_compound:
            self.display.DisplayShape(shape, color=self.base_color, transparency=0.7)
        for shape in self.current_compound:
            self.display.DisplayShape(shape, color=self.current_color)

    def display_assembly_graph(self, graph, assembly_sequence):
        plt.close()
        current_task = assembly_sequence[self.current_assembly_sequence_index]
        print(f'current_task: {current_task}')

        #todo: highlight current task in graph
        self.update_color_map(graph, current_task)
        pos = nx.multipartite_layout(graph, subset_key='tier')
        nx.draw(graph, pos=pos, node_color=self.color_map, with_labels=True)
        plt.show()

    def init_color_map(self, graph):
        color_map = []
        for node in graph:
            color_map.append('green')
        self.color_map = color_map

    def update_color_map(self, graph, node_idx):
        for count, node in enumerate(graph):
            if node == node_idx:
                self.color_map[count] = 'red'

    def highlight_node_in_graph(self, graph, node_idx):
        color_map = []
        for node in graph:
            if node == node_idx:
                color_map.append('red')
            else:
                color_map.append('green')
        return color_map

    def start_main_loop(self):
        self.start_display()


class Model():
    def __init__(self, full_compound, assembly_tree_graph):
        self.full_compound = full_compound
        self.assembly_tree_graph = assembly_tree_graph
        self.init_assembly_tiers()
        self.add_tier_to_nodes()
        self.get_random_linear_assembly_sequence()

    def init_assembly_tiers(self):
        tier_list = []
        initial_node = 'Start'
        tier_list.append([initial_node])
        current_tier = 0
        while True:
            next_tier_nodes = []
            next_neighbor_nodes = get_next_neighbor_nodes(self.assembly_tree_graph, tier_list[current_tier])
            if next_neighbor_nodes != []:
                for node in next_neighbor_nodes:
                    if self.predecessors_already_visited(node, tier_list, self.assembly_tree_graph):
                        next_tier_nodes.append(node)
                tier_list.append(next_tier_nodes)
                current_tier += 1
            else:
                break
        self.assembly_tiers = tier_list

    def predecessors_already_visited(self, node, tier_list, G):
        flatten_tier_list = list(chain.from_iterable(tier_list))
        for predecessor in G.predecessors(node):
            if predecessor in flatten_tier_list:
                return True
            else:
                return False

    def add_tier_to_nodes(self):
        for counter, tier in enumerate(self.assembly_tiers):
            for elem in tier:
                if elem in self.assembly_tree_graph.nodes():
                    self.assembly_tree_graph.nodes[elem]['tier'] = counter

    def get_initial_process(self, assembly_compound):
        node_id = self.assembly_tiers[1]
        node_id = node_id[0]
        sub_1 = self.assembly_tree_graph.nodes[node_id]['sub_1']
        sub_1 = sub_1[0]
        assembly_compound = append_shape_to_compound(self.full_compound, assembly_compound, sub_1)

    def get_random_linear_assembly_sequence(self):
        assembly_sequence = []
        for tier in self.assembly_tiers:
            shuffle(tier)
            for elem in tier:
                assembly_sequence.append(elem)
        print(f'random assembly seqeuence: {assembly_sequence}')
        self.assembly_sequence = assembly_sequence


class Controller():
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def start(self):
        self.view.setup(self)
        self.view.start_main_loop()


    def init_part(self):
        self.model.get_initial_process(self.view.assembly_compound)
        self.view.display.EraseAll()
        self.view.display_assembly_compound()
        self.view.init_color_map(self.model.assembly_tree_graph)
        self.view.display_assembly_graph(self.model.assembly_tree_graph, self.model.assembly_sequence)

    def next_tier(self):
        self.view.current_assembly_tier += 1
        self.view.update_compounds_with_tiers(assembly_tiers=self.model.assembly_tiers, full_compound=self.model.full_compound)
        self.view.display.EraseAll()
        self.view.display_assembly_compound()

    def next_part(self):
        self.view.current_assembly_sequence_index += 1
        self.view.update_compounds_with_sequence(assembly_sequence=self.model.assembly_sequence, full_compound=self.model.full_compound, assembly_tree_graph=self.model.assembly_tree_graph)
        self.view.display.EraseAll()
        self.view.display_assembly_compound()
        self.view.display_assembly_graph(self.model.assembly_tree_graph, self.model.assembly_sequence)
        #print(f'current_task: {self.model.assembly_sequence[self.view.current_assembly_sequence_index]}')



# ------------------
# Utility functions
# ------------------

def append_shape_to_compound(aResCompound, current_compound, node):
    node_id = node
    for shape in aResCompound:
        label, color = aResCompound[shape]
        if str(node_id) in label:
            current_compound[shape] = [label, color]
            break
    return current_compound

# get successors
def get_next_neighbor_nodes(g, current_tier_nodes):
    next_tier_nodes = []
    for node in current_tier_nodes:
        for neighbor in g.neighbors(node):
            if neighbor not in next_tier_nodes:
                if neighbor != node:
                    next_tier_nodes.append(neighbor)
    return next_tier_nodes

def plot_graph(G):
    pos = nx.multipartite_layout(G, subset_key='layer')
    plt.figure(figsize=(8, 8))
    nx.draw(G, pos, with_labels=True)
    plt.axis("equal")
    plt.show()

# ----------------
# Main Loop
# ----------------

def main():
    product_name = 'centrifugal_pump'
    step_file_path = '../../data/stepFiles/' + product_name + '.stp'

    # Load 3D model
    aResCompound = read_step_file_with_names_colors(step_file_path)
    aResCompound = rename_shapes_with_numbers(aResCompound)
    aResCompound = remove_fasteners(aResCompound)

    # Load precedence graph
    pickle_path = '../../out/assembly_tree/' + product_name + '_from_pg.pickle'
    #pickle_path = '../../out/assembly_tree/' + product_name + '_translated.pickle'
    with open(pickle_path, 'rb') as input_file:
        assembly_tree_graph = pickle.load(input_file)

    c = Controller(Model(aResCompound, assembly_tree_graph), View())
    c.start()


if __name__ == '__main__':
    main()