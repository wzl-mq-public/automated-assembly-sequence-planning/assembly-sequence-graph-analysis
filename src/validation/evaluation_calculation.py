# Script to calculate operational flexibility values
import pickle
import numpy as np
import networkx as nx
import operator as op
from functools import reduce
iso = nx.algorithms.isomorphism

from src.graph_utilities.graph_builder import *

test_graph = {1: ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 6], [3]),
         2: ([1, 2, 4, 5, 6], [1, 2, 5, 6], [4]),
         3: ([1, 2, 4, 5, 6], [1, 4, 5], [2, 6]),
         4: ([1, 2, 5, 6], [1, 5], [2, 6]),
         5: ([1, 4, 5], [1, 5], [4]),
         6: ([1, 4, 5], [1, 4], [5]),
         7: ([1, 4], [1], [4]),
         8: ([1, 5], [1], [5]),
         9: ([2, 6], [2], [6])}


def open_aog_file(product_name):
    pickle_path = '../../data/and_or_pickles/' + product_name + '.pickle'
    with open(pickle_path, 'rb') as input_file:
        and_or_graph = pickle.load(input_file)
    return and_or_graph

def open_assembly_tree(product_name):
    pickle_path = '../../out/assembly_tree/' + product_name + '.pickle'
    with open(pickle_path, 'rb') as input_file:
        assembly_tree = pickle.load(input_file)
    return assembly_tree

# ----------------------------------------
# Functions to calc number of PGs from AOG
# ----------------------------------------

def calc_number_of_possible_PGs(AOG, plot_tree=False):
    flipped_duplicates = find_duplicate_keys(AOG)
    reduced_AOG = remove_duplicates(AOG, flipped_duplicates)
    flipped_reduced_AOG = flip_reduced_AOG(reduced_AOG)

    simple_nodes = list(flipped_reduced_AOG.values())
    decision_tree = get_decision_tree(AOG, flipped_reduced_AOG, flipped_duplicates, simple_nodes)
    if plot_tree==True:
        color_map = get_color_map(decision_tree, simple_nodes)
        plot_decision_tree(decision_tree, color_map)

    num_end_points = calc_end_points(decision_tree)
    return num_end_points

def estimate_number_of_possible_PGs(n_aog_edges):
    # Formula determined by exponential regression analysis
    y = 396.294 * np.exp(0.00268 * n_aog_edges)
    return y

def remove_duplicates(AOG, flipped_duplicates):
    reduced_AOG = {}
    for key, val in AOG.items():
        if str(val[0]) not in list(flipped_duplicates.keys()):
            reduced_AOG[key] = val
    return reduced_AOG

def flip_reduced_AOG(reduced_AOG):
    flipped_reduced_AOG = {}
    for key, val in reduced_AOG.items():
        flipped_reduced_AOG[str(val[0])] = key
    return flipped_reduced_AOG

def calc_end_points(decision_tree):
    end_points = [node for node, out_degree in decision_tree.out_degree() if out_degree == 0]
    num_end_points = len(end_points)
    return num_end_points

def find_duplicate_keys(graph):
    rev_multidict = {}
    for key, value in graph.items():
        rev_multidict.setdefault(str(value[0]), set()).add(key)
    duplicate_values = [key for key, values in rev_multidict.items() if len(values) > 1]

    duplicate_keys = {}
    for key, value in graph.items():
        if str(value[0]) in duplicate_values:
            duplicate_keys.setdefault(str(value[0]), set()).add(key)
    return duplicate_keys

# -----------------------------------------------
# Functions to calc number of sequences from PG
# -----------------------------------------------

def calc_number_of_possible_sequences(assembly_tree):
    # calc with slot block theory (https://www.sciencedirect.com/science/article/abs/pii/S0166361597001036?via%3Dihub)
    sorted_node_list = get_topological_sort_list_by_assembly_tiers(assembly_tree)

    # building parse_tree
    parse_tree = build_parse_tree(assembly_tree, sorted_node_list)

    # visual check
    #pos = hierarchy_pos(parse_tree, 's_INIT')
    #nx.draw(parse_tree, pos, with_labels=True)
    #plt.show()

    # calc number of sequences from parse tree
    num_sequences = calc_num_sequences(parse_tree)
    return num_sequences

def calc_num_sequences(parse_tree):
    parse_tree.remove_node('Start')
    num_sequences = calc_num_plans_of_serial_node(parse_tree, 's_INIT')
    return num_sequences

def calc_num_plans_of_parallel_node(parse_tree, node):
    # get all successors of parallel node
    num_plans = 1
    successors = list(parse_tree.successors(node))

    # calc combination of first pair
    n = dfs_num_of_all_successing_operations(parse_tree, successors[0])
    m = dfs_num_of_all_successing_operations(parse_tree, successors[1])
    n_plans_of_first_pair = total_num_combinations_of_parallel_branch(n, m)
    num_plans = num_plans * n_plans_of_first_pair

    if 's_' in str(successors[0]):
        sub_plans = calc_num_plans_of_serial_node(parse_tree, successors[0])
        num_plans = num_plans * sub_plans
    if 's_' in str(successors[1]):
        sub_plans = calc_num_plans_of_serial_node(parse_tree, successors[1])
        num_plans = num_plans * sub_plans

    visited = [successors[0], successors[1]]

    # multiply intermediate result with next successors
    for i in range(2, len(successors)):
        n = 0
        for elem in visited:
            n += dfs_num_of_all_successing_operations(parse_tree, elem)
        m = dfs_num_of_all_successing_operations(parse_tree, successors[i])
        num_plans = num_plans * total_num_combinations_of_parallel_branch(n, m)

        # multiply with num of sub_plans in case it is a sequential node
        if 's_' in str(successors[i]):
            sub_plans = calc_num_plans_of_serial_node(parse_tree, successors[i])
            num_plans = num_plans * sub_plans
        visited.append(successors[i])
    return num_plans

def calc_num_plans_of_serial_node(parse_tree, node):
    num_plans = 1
    successors = list(parse_tree.successors(node))

    # get max of calced plans
    for successor in successors:
        if 'p_' in str(successor):
            next_level_successors = list(parse_tree.successors(successor))
            if next_level_successors != []: #workaround for parallel nodes with moved nodes
                plans = calc_num_plans_of_parallel_node(parse_tree, successor)
                if plans > num_plans:
                    num_plans = plans

    return num_plans

def bfs_calc_subplans(visited, graph, node):
    visited.append(node)
    queue = []
    queue.append(node)

    while queue:
        s = queue.pop(0)
        if 's_' in str(s):
            print(s)
        if 'p_' in str(s):
            print(s)

        for successor in graph.successors(s):
            if successor not in visited:
                visited.append(successor)
                queue.append(successor)

def dfs_num_of_all_successing_operations(parse_tree, local_root):
    visited = set()
    operations = []
    dfs_operations(visited, operations, parse_tree, local_root)
    return len(operations)

def dfs_operations(visited, operations, graph, node):
    if node not in visited:
        visited.add(node)
        if is_operation(node):
            operations.append(node)
        for successor in graph.successors(node):
            dfs_operations(visited, operations, graph, successor)

def is_operation(node):
    if 's_' in str(node):
        return False
    if 'p_' in str(node):
        return False
    else:
        return True

def total_num_combinations_of_parallel_branch(n, m):
    sum = 0
    lim = min(n+1, m)
    for i in range(1, lim+1):
        C1 = nCr(n+1, i)
        C2 = nCr(m-1, i-1)
        sum = sum + C1 * C2
    return sum

def nCr(n, r):
    r = min(r, n - r)
    numer = reduce(op.mul, range(n, n - r, -1), 1)
    denom = reduce(op.mul, range(1, r + 1), 1)
    return numer // denom

# -------------------------

def calc_total_number_of_sequences(AOG):
    pass

