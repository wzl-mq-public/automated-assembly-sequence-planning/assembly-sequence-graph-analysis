import networkx as nx

from src.validation.evaluation_calculation import open_aog_file, sort_by_values_len, calc_number_of_possible_PGs, calc_number_of_possible_sequences, open_assembly_tree
from src.validation.uniqueness_of_aog_pg_sequences import get_unique_assembly_tree_graphs, calc_num_sequences_per_atg
from src.graph_utilities.aog_to_assembly_tree import get_monte_carlo_optimal_assembly_tree
import sys
import matplotlib.pyplot as plt
import numpy as np


def calc_avg_num_sequences(AOG, iterations, n_possible_pg):
    unique_atg = get_unique_assembly_tree_graphs(AOG, iterations, n_possible_pg)
    num_sequences_block = []
    for atg in unique_atg:
        num_sequences_block.append(calc_number_of_possible_sequences(atg))
    avg_num_sequence = sum(num_sequences_block) / len(num_sequences_block)
    return avg_num_sequence

def calc_mean_num_sequences(AOG, iterations, n_possible_pg):
    unique_atg = get_unique_assembly_tree_graphs(AOG, iterations, n_possible_pg)
    num_sequences_block = []
    for atg in unique_atg:
        num_sequences_block.append(calc_number_of_possible_sequences(atg))
    mean = np.mean(num_sequences_block)
    return round(mean)

def calc_std_of_sequences(AOG, iterations, n_possible_pg):
    unique_atg = get_unique_assembly_tree_graphs(AOG, iterations, n_possible_pg)
    num_sequences_block = []
    for atg in unique_atg:
        num_sequences_block.append(calc_number_of_possible_sequences(atg))
    std = np.std(num_sequences_block)
    return std

def plot_histogram_num_sequences(AOG, iterations, n_possible_pg, save_fig=False, save_path="histogram.png"):
    plt.close()
    unique_atg = get_unique_assembly_tree_graphs(AOG, iterations, n_possible_pg)
    num_sequences_block = []
    for atg in unique_atg:
        num_sequences_block.append(calc_number_of_possible_sequences(atg))
    bins = calc_bin_for_histogram(num_sequences_block)
    plt.hist(num_sequences_block, bins=bins)
    plt.ylabel('counts')
    plt.xlabel('sequences')
    if save_fig:
        plt.savefig(save_path)
    plt.show()
    plt.close()


def calc_bin_for_histogram(x):
    q25, q75 = np.percentile(x, [25, 75])
    bin_width = 2 * (q75 - q25) * len(x) ** (-1 / 3)
    if bin_width == 0.0:
        bin_width = 1
    bins = round((max(x) - min(x)) / bin_width)
    return bins

def sample_required(n, error_margin):
    standard_deviation = 0.5 #according to https://de.wikihow.com/Die-Stichprobengr%C3%B6%C3%9Fe-berechnen
    z = 1.96 # for confidency of 95%
    upper = (z**2 * standard_deviation*(1-standard_deviation))/error_margin**2
    lower = 1 + ((z**2 * standard_deviation*(1-standard_deviation))/(error_margin**2 * n))
    sample_size = upper/lower
    return round(sample_size)

def estimate_total_num_of_sequences(AOG, num_possible_pg, sample_size):
    mean = calc_mean_num_sequences(AOG, sample_size, num_possible_pg)
    #print(f'mean_num_sequences: {mean}')

    estimate_num_sequences = round(num_possible_pg * mean)
    return estimate_num_sequences

def calc_responsiveness(num_sequences, data_size):
    return num_sequences / data_size
