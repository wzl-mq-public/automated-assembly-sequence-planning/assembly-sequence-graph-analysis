from src.validation.evaluation_calculation import calc_number_of_possible_PGs, calc_number_of_possible_sequences
from src.graph_utilities.aog_to_assembly_tree import get_assembly_tree_random
from src.graph_utilities.graph_helper import get_random_sequence_from_PG
import networkx as nx
from matplotlib import pyplot as plt


test_graph_3 = {
    1: ([1,2,3,4,5], [1,2,3,5], [4]),
    2: ([1,2,3,4,5], [1,2,3,4], [5]),
    3: ([1,2,3,5], [1,2,3], [5]),
    4: ([1,2,3,4], [1,2,3], [4]),
    5: ([1,2,3], [1], [2,3]),
    6: ([1,2,3], [2], [1,3]),
    7: ([2,3], [2], [3]),
    8: ([1,3], [2], [3])
}


def all_sequences_unique(AOG, n_possible_pg, max_iter=100):
    unique_atg = get_unique_assembly_tree_graphs(AOG, max_iter, n_possible_pg)
    num_sequences = calc_num_sequences_per_atg(unique_atg)
    all_sequences = get_all_sequences(unique_atg, num_sequences, max_iter)
    all_unique = all_elem_unique(all_sequences)
    return all_unique

def get_unique_assembly_tree_graphs(AOG, max_iter, n_possible_pg):
    unique_atg_jsons = get_all_unique_atg_jsons(AOG, max_iter, n_possible_pg)
    unique_atg = get_all_unique_atg(unique_atg_jsons)
    return unique_atg

def get_all_unique_atg_jsons(AOG, max_iter, n_possible_pg):
    #print(f'num_ATGs: {num_atgs}')
    unique_atg_jsons = []
    iter_counter = 0
    while (len(unique_atg_jsons) < n_possible_pg) and (iter_counter < max_iter):
        atg = get_assembly_tree_random(AOG)
        atg_json = nx.readwrite.json_graph.adjacency_data(atg)
        if atg_json not in unique_atg_jsons:
            unique_atg_jsons.append(atg_json)
        iter_counter += 1
    return unique_atg_jsons

def get_all_unique_atg(unique_atg_jsons):
    unique_atg = []
    for atg_json in unique_atg_jsons:
        graph = nx.readwrite.json_graph.adjacency_graph(atg_json)
        #graph.remove_edge('Start', 'Start')
        unique_atg.append(graph)
    return unique_atg

def calc_num_sequences_per_atg(unique_atg):
    num_sequences = []
    for PG in unique_atg:
        seq = calc_number_of_possible_sequences(PG)
        num_sequences.append(seq)
    #print(f'num_sequences = {num_sequences}')
    return num_sequences

def get_all_sequences(unique_atg, num_sequences, max_iter):
    all_sequences = []
    for idx, atg in enumerate(unique_atg):
        unique_sequences = []
        max_sequences = num_sequences[idx]
        iter_counter = 0
        while (len(unique_sequences) < max_sequences) and (iter_counter < max_iter):
            random_sequence = get_random_sequence_from_PG(atg)
            if random_sequence not in unique_sequences:
                unique_sequences.append(random_sequence)
            iter_counter += 1
        all_sequences.append(unique_sequences)
    #print(f'all_sequences: {all_sequences}')
    return all_sequences


def all_elem_unique(all_sequences):
    unique_united_list = []
    all_sequence_counter = 0
    unique_sequence_counter = 0
    for atg_seqs in all_sequences:
        for seq in atg_seqs:
            all_sequence_counter += 1
            if seq not in unique_united_list:
                unique_united_list.append(seq)
                unique_sequence_counter += 1
    if all_sequence_counter == unique_sequence_counter:
        return True
    else:
        return False

def main():
    all_unique = all_sequences_unique(test_graph_3, max_iter=100)
    print(f'all sequences unique? {all_unique}')


if __name__ == '__main__':
    main()

