import sys

import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
import logging
from statistics import mean

from src.graph_utilities.and_or_graph_generator_bottom_up import *
from src.graph_utilities.graph_helper import sort_by_values_len, get_topological_sort_list_by_assembly_tiers, \
    init_assembly_tiers_multiple_start_nodes
from src.graph_utilities.pg_to_assembly_tree import *
from src.validation.evaluation_calculation import estimate_number_of_possible_PGs
from src.validation.responsiveness_calculation import *
from src.graph_utilities.graph_helper import estimate_num_sequences, init_assembly_tiers, update_at_df_with_tier_list
import os
import time

def open_aog_pickle(path):
    with open(path, 'rb') as input_file:
        and_or_graph = pickle.load(input_file)
    return and_or_graph

def assembly_tier_pg_xlsx_to_df(path):
    pg_df = pd.read_excel(path, header=None)

    # add product names to pg_df
    at_df = read_assembly_tiers(path)
    product_index_list = at_df['Product']

    pg_df.columns = product_index_list
    pg_df.index = product_index_list

    return pg_df

def read_assembly_tiers(path):
    at_xl_file = pd.ExcelFile(path)
    at_df = at_xl_file.parse('Assembly Directions', header=None, index_col=None)
    at_df.fillna(value=0, inplace=True)
    at_df = at_df[[0, 7]]
    new_header = at_df.iloc[0]
    at_df = at_df[1:]
    at_df.columns = new_header
    return at_df

def pg_df_to_assembly_tree(pg_df, at_df):
    pg = nx.from_pandas_adjacency(pg_df, create_using=nx.DiGraph())

    initial_parts = get_all_initial_parts(pg)
    logging.info(f'initial part: {initial_parts}')

    for elem in initial_parts:
        pg.add_edge('Start', elem)

    pg.nodes['Start']['layer'] = 0
    pg.nodes['Start']['tier'] = 0

    # add assembly tiers as layer
    initial_part = 'Start'
    tier_list = init_assembly_tiers(pg)
    at_df = update_at_df_with_tier_list(at_df, tier_list)
    add_assembly_tiers_to_graph(pg, at_df)

    mapped_data = init_mapped_data_with_sub_2(pg)

    # sort by layer
    sorted_node_list = get_topological_sort_list_by_assembly_tiers(pg, initial_node='Start')
    pg_sorted = nx.DiGraph()
    pg_sorted.add_nodes_from(sorted_node_list)
    node_attributes = {node:data for node, data in pg.nodes(data=True)}
    for key, value in node_attributes.items():
        pg_sorted.nodes[key]['layer'] = value['layer']
    pg_sorted.add_edges_from(pg.edges())

    pg_sorted = relabel_all_nodes(pg_sorted)

    #nx.draw(pg_sorted, with_labels=True)
    #plt.show()

    mapped_data = get_first_tier_node_info(pg_sorted, mapped_data, 'Start')
    mapped_data = get_node_info_for_other_tiers(pg_sorted, mapped_data)
    pg = add_mapped_data_to_nodes(pg_sorted, mapped_data)
    return pg

def add_assembly_tiers_to_graph(pg, at_df):
    for idx, row in at_df.iterrows():
        product = row['Product']
        layer = row['Assembly Tier']
        pg.nodes[product]['layer'] = layer
        pg.nodes[product]['tier'] = layer

def save_graph_to_pickle(path, G):
    with open(path, 'wb') as handle:
        pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)

def plot_and_save_atg_from_pg(atg_from_pg, product_name, write_path):
    pos = []
    try:
        pos = nx.multipartite_layout(atg_from_pg, subset_key='layer')
    except:
        print("Error while calcing pos for max_flex_atg")
    plt.figure(figsize=(8, 8))
    if pos != []:
        nx.draw(atg_from_pg, pos=pos, with_labels=True)
    else:
        nx.draw(atg_from_pg, with_labels=True)
    plt.axis("equal")
    plt.savefig(os.path.join(write_path, product_name + '_atg_from_pg.png'))
    plt.show()
    plt.close()


def main():
    dir_path = os.path.join('C:\\', 'Users', 'adam-uqef35nm77fzn5b', 'Sciebo', '03_Dissertation', '50_Versuche', 'data')
    read_path = os.path.join(dir_path, "RQ1_v6")
    write_path = os.path.join(dir_path, "RQ1_v6")

    product_name = "ex_rq1_10_cutting_bend"

    log_filename = os.path.join(write_path, product_name + '_reduce_graph_py.log')
    logging.basicConfig(
        format='%(levelname)s | %(asctime)s - %(message)s',
        level=logging.INFO,
        handlers=[
            logging.FileHandler(log_filename),
            logging.StreamHandler()
        ]
    )
    # -------------------------
    # component based PG to ATG
    # -------------------------
    logging.info('--- PG to ATG analysis started ---')
    pg_path = os.path.join(read_path, product_name + '_AssemblyTiers.xlsx')
    pg_df = assembly_tier_pg_xlsx_to_df(pg_path)
    at_df = read_assembly_tiers(pg_path)

    # Check if PG is complete
    pg = nx.from_pandas_adjacency(pg_df, create_using=nx.DiGraph())
    n_nodes_total = pg.number_of_nodes()
    print(f'n_nodes_total: {n_nodes_total}')

    isolated_nodes = list(nx.isolates(pg))
    n_isolates = len(isolated_nodes)
    print(f'n_isolates: {n_isolates}')
    print(f'isolate_nodes: {isolated_nodes}')

    nx.draw(pg, with_labels=True)
    plt.show()

    graph_reduction = input("Do you want to eliminate nodes? (y/n): ")

    if graph_reduction == 'y':
        logging.info('Remove nodes from data manually...')
        input_string = input("Enter nodes to remove (separated by space): ")
        remove_list = input_string.split(" ")
        print(f'remove_list: {remove_list}')

        #remove from pg_df, at_df
        translation_df = at_df.copy()
        print(pg_df)
        pg_df.drop(remove_list, axis=0, inplace=True)
        pg_df.drop(remove_list, axis=1, inplace=True)
        print(pg_df)
        print(at_df)
        for elem in remove_list:
            at_df.drop(at_df.index[at_df['Product']==elem], inplace=True)
        print(at_df)
        pg.remove_nodes_from(remove_list)
        n_nodes_total = pg.number_of_nodes()
        print(f'n_nodes_total: {n_nodes_total}')



    logging.info('Start transforming pg to atg.')
    start = time.perf_counter()
    atg_from_pg = pg_df_to_assembly_tree(pg_df, at_df)
    stop = time.perf_counter()
    t_pg_to_atg = stop - start
    print(f't_pg_to_atg: {t_pg_to_atg}')

    # Save generated atg graph
    save_path = os.path.join(write_path, product_name + '_atg_from_pg.pickle')
    save_graph_to_pickle(save_path, atg_from_pg)

    # Plot atg_from_pg
    plot_and_save_atg_from_pg(atg_from_pg, product_name, write_path)




    atg_from_pg_seq = calc_number_of_possible_sequences(atg_from_pg)
    print(f'atg_from_pg num of sequences: {atg_from_pg_seq}')
    is_num_seq_pg_precise = True
    '''
    except:
        logging.warning('Could not calc num of sequences for pg')
        logging.info('Brute force calculation started')
        # TODO calc brute force
        atg_from_pg_seq = estimate_num_sequences(atg_from_pg, n_nodes_total * n_nodes_total)
        print(f'atg_from_pg num of sequences: {atg_from_pg_seq}')
        is_num_seq_pg_precise = False
    '''
    data_atg_from_pg = sys.getsizeof(atg_from_pg)

    # ------------------------
    # MW & LW to AOG to ATG
    # ------------------------
    logging.info('--- AOG to ATG analysis started ---')

    if graph_reduction == 'y':
        logging.info('Remove same nodes from MW and Liaisons.')
        liaisons_file = os.path.join(read_path, product_name + "_Liaisons.xlsx")
        mw_path = os.path.join(read_path, product_name + "_Moving wedge.xlsx")
        liaison_df = read_liaison_data_excel(liaisons_file)
        mw_dfs = read_mw_data_excel(mw_path)

        # get indexes to remove
        remove_list_index = []
        for elem in remove_list:
            index_to_remove = translation_df.index[translation_df['Product'] == elem].tolist()
            remove_list_index.append(index_to_remove[0])
        remove_list_index = [x - 1 for x in remove_list_index]
        print(f'remove_list_index: {remove_list_index}')

        # remove column & rows from df
        liaison_df.drop(remove_list_index, axis=0, inplace=True)
        liaison_df.drop(remove_list_index, axis=1, inplace=True)
        mw_x = mw_dfs['MW_x']
        mw_y = mw_dfs['MW_y']
        mw_z = mw_dfs['MW_z']
        mw = [mw_x, mw_y, mw_z]
        for elem in mw:
            elem.drop(remove_list_index, axis=0, inplace=True)
            elem.drop(remove_list_index, axis=1, inplace=True)

        # save reduced matrices as xlsx
        liaisons_path_reduced = os.path.join(read_path, product_name + "_Liaisons_reduced.xlsx")
        mw_path_reduced = os.path.join(read_path, product_name + "_Moving wedge_reduced.xlsx")
        liaison_df.to_excel(liaisons_path_reduced, sheet_name='Liaison Matrix', header=False, index=False)

        with pd.ExcelWriter(mw_path_reduced) as writer:
            mw[0].to_excel(writer, sheet_name='MW_x', header=False, index=False)
            mw[1].to_excel(writer, sheet_name='MW_y', header=False, index=False)
            mw[2].to_excel(writer, sheet_name='MW_z', header=False, index=False)
        logging.info('Reduced MW and Liaison saved as XLSX.')


    # create AOG bu from liaison & mw
    logging.info('Start generating AOG from MW and Liaisons')
    restrict_nonstat_size = False
    max_nonstat_parts = 3
    if graph_reduction == 'y':
        result = run_experiment_xlsx(read_path, write_path, product_name, restrict_nonstat_size=restrict_nonstat_size, max_nonstat_parts=max_nonstat_parts, reduced=True)
    else:
        result = run_experiment_xlsx(read_path, write_path, product_name, restrict_nonstat_size=restrict_nonstat_size,
                                     max_nonstat_parts=max_nonstat_parts, reduced=False)
    print(result)
    logging.info('Saved AOG as pickle file.')

    # Calc AOG values
    logging.info('Load AOG from pickle file.')
    aog_path = os.path.join(read_path, product_name + "_AOG.pickle")
    G = open_aog_pickle(aog_path)
    G_sorted = sort_by_values_len(G)

    n_parts_in_aog = len(G_sorted[0][0])

    # Close unused variables to free memory
    del(G)
    del(pg)
    del(pg_df)
    del(at_df)

    logging.info('Calc number of possible PGs from AOG...')
    n_aog_edges = result['edge_count']
    logging.info(f'n_aog_edges: {n_aog_edges}')
    if n_aog_edges < 2200:
        is_n_possible_pg_exact = True
        num_possible_pg = calc_number_of_possible_PGs(G_sorted)
    else:
        logging.warning('number of possible PGs just estimated, because computing complexity too high.')
        is_n_possible_pg_exact  = False
        num_possible_pg = estimate_number_of_possible_PGs(n_aog_edges)


    logging.info('Calc sample size for total num_sequence_estimation...')
    sample_size = sample_required(num_possible_pg, 0.05)
    logging.info(f'sample_size: {sample_size}')

    logging.info('Estimate total num_of_sequences for AOG...')
    total_num_sequences = estimate_total_num_of_sequences(G_sorted, num_possible_pg, sample_size)
    print(f'total_num_sequences: {total_num_sequences}')

    data_size_aog = sys.getsizeof(G_sorted)
    print(f'data size of AOG: {data_size_aog}')


    logging.info('Create histrogram...')
    plot_histogram_num_sequences(G_sorted, sample_size, num_possible_pg, save_fig=True,
                                 save_path=os.path.join(write_path, product_name + '_n_seq_histogram.png'))

    logging.info('Find an optimal ATG from AOG...')
    start = time.perf_counter()
    max_flex_atg = get_monte_carlo_optimal_assembly_tree(G_sorted, sample_size, opt_goal='high_flex')
    stop = time.perf_counter()
    t_aog_to_atg = stop - start
    print(f't_aog_to_atg: {t_aog_to_atg}')

    # Save generated atg graph
    save_path = os.path.join(write_path, product_name + '_atg_from_aog_max_flex.pickle')
    save_graph_to_pickle(save_path, max_flex_atg)

    max_seq = calc_number_of_possible_sequences(max_flex_atg)
    print(f'max number of sequences: {max_seq}')

    data_max_flex_atg = sys.getsizeof(max_flex_atg)
    print(f'data size of max_flex_atg: {data_max_flex_atg}')


    # Plot max_flex_atg
    max_flex_atg.nodes['Start']['layer'] = 0
    tier_list = init_assembly_tiers(max_flex_atg)
    for i, tier in enumerate(tier_list):
        for node in tier:
            max_flex_atg.nodes[node]['layer'] = i

    pos = []
    try:
        pos = nx.multipartite_layout(max_flex_atg, subset_key='layer')
    except:
        logging.warning('Calcing pos for max_flex_atg not possible.')
    plt.figure(figsize=(8, 8))
    if pos != []:
        nx.draw(max_flex_atg, pos=pos, with_labels=True)
    else:
        nx.draw(max_flex_atg, with_labels=True)
    plt.axis("equal")
    plt.savefig(os.path.join(write_path, product_name + '_atg_max_flex.png'))
    plt.show()
    plt.close()

    # Save data to excel
    result_data = [
        ["VERIFICATION", ""],
        ["n_nodes_total", n_nodes_total],
        ["n_nodes_isolated", n_isolates],
        ["n_subgraphs", ""],
        ["n_nodes_min_subgraph", ""],
        ["n_nodes_avg_subgraph", ""],
        ["n_nodes_max_subgraph", ""],
        ["is_num_seq_pg_precise", ""],
        ["", ""],
        ["PG TO ATG ANALYSIS", ""],
        ["n_seq", atg_from_pg_seq],
        ["t_gen [s]", t_pg_to_atg],
        ["data_size [byte]", data_atg_from_pg],
        ["", ""],
        ["AOG TO ATG ANALYSIS", ""],
        ["algorithm_type", "bottom_up"],
        ["restrict_non_stat_size", restrict_nonstat_size],
        ["max_nonstat_parts", max_nonstat_parts],
        ["t_gen_aog [s]", result['runtime']],
        ["n_parts_in_aog", n_parts_in_aog],
        ["n_nodes_aog", result['node_count']],
        ["n_edges_aog", result['edge_count']],
        ["n_possible_pg", num_possible_pg],
        ["n_possible_pg_exact", is_n_possible_pg_exact],
        ["sample_size", sample_size],
        ["n_seq_aog_total", total_num_sequences],
        ["data_size_aog [byte]", data_size_aog],
        ["", ""],
        ["Find optimal ATG from AOG", ""],
        ["n_seq", max_seq],
        ["t_gen [s]", t_aog_to_atg],
        ["data_size", data_max_flex_atg]
    ]
    logging.info('Save results to excel file.')
    result_data_df = pd.DataFrame(result_data)
    result_data_df.to_excel(os.path.join(write_path, product_name + '_graph_analysis_results.xlsx'))
    print("Done.")

if __name__ == '__main__':
    #import cProfile
    #import pstats

    #pr = cProfile.Profile()
    #pr.enable()

    main()

    #pr.disable()
    #stats = pstats.Stats(pr)
    #stats.sort_stats(pstats.SortKey.TIME)
    #stats.dump_stats(filename='rq1_script_profiling.prof')









