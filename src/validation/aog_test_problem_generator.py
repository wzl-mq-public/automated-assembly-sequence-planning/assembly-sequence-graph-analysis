# Tool to generate artificial data for performance analysis

import numpy as np
import pandas as pd
from networkx.generators.random_graphs import erdos_renyi_graph
from networkx import convert_matrix

def calc_liaisons_count(liaison_df):
    return liaison_df.to_numpy().sum()/2

def calc_MWF(mw_df):
    return mw_df.to_numpy().sum() / (len(mw_df.index)*len(mw_df.index))

def generate_liaison_csv_file(path, product_name, n_parts, p_liaisons):
    save_path = path + product_name + '_Liaisons.csv'
    liaison_df = generate_liaison_df(n_parts, p_liaisons)
    liaison_df.to_csv(save_path)

def generate_liaison_df(n_parts, p_liaisons):
    #p = n_liaisons * 2 / (n_parts * n_parts - n_parts)
    p = p_liaisons
    g_liaisons = erdos_renyi_graph(n_parts, p)
    liaison_numpy = convert_matrix.to_numpy_matrix(g_liaisons)
    liaison_numpy = liaison_numpy.astype(int)
    liaison_df = pd.DataFrame(liaison_numpy)
    return liaison_df

def generate_mw_csv_files(path, product_name, n_parts, MWF):
    path_x = path + product_name + '_Moving wedge_x.csv'
    path_y = path + product_name + '_Moving wedge_y.csv'
    path_z = path + product_name + '_Moving wedge_z.csv'
    mw_df_x = generate_mw_matrix(n_parts, MWF)
    mw_df_y = generate_mw_matrix(n_parts, MWF)
    mw_df_z = generate_mw_matrix(n_parts, MWF)
    mw_df_x.to_csv(path_x)
    mw_df_y.to_csv(path_y)
    mw_df_z.to_csv(path_z)

def generate_mw_matrix(n_parts, MWF):
    #mw_matrix = np.zeros((n_parts, n_parts))
    mw_matrix = np.random.choice([0,1], size=(n_parts, n_parts), p=[1-MWF, MWF])
    mw_df = pd.DataFrame(mw_matrix)
    return mw_df

def generate_files(path, product_name, n_parts, p_liaisons, mwf):
    generate_liaison_csv_file(path=path, product_name=product_name, n_parts=n_parts, p_liaisons=p_liaisons)
    generate_mw_csv_files(path=path, product_name=product_name, n_parts=n_parts, MWF=mwf)

if __name__ == '__main__':
    #DOE2 Box-Behnken
    path = '../../data/mw_liaison_csv_generated/screening/'
    generate_files(path=path, product_name='pre_exp_1', n_parts=17, p_liaisons=0.14, mwf=0.86)


