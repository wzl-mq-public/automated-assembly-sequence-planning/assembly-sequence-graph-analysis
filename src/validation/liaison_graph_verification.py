"""
For AOG validation, first it is necessary to verify that the liaison graph
@author: Sören Münker (mnk)
"""

import os
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt

folder_path = os.path.abspath('C:/Users/...') #Insert absolute path

product_name = 'ex_rq1_1_centrifugal_pump'

liaison_file_name = product_name + '_Liaisons.xlsx'
bb_file_name = product_name + '_BB Volumes.xlsx'


liaison_file_path = os.path.join(folder_path, liaison_file_name)
liaisons_df = pd.read_excel(liaison_file_path, index_col=None, header=None)

bb_file_path = os.path.join(folder_path, bb_file_name)
part_name_df = pd.read_excel(bb_file_path)
part_names = list(part_name_df['Product'].values)

liaisons_df.index = part_names
liaisons_df.columns = part_names

G = nx.from_pandas_adjacency(liaisons_df)

nx.draw(G, with_labels=True)
plt.savefig(os.path.join(folder_path, product_name + '_liaison_graph.png'))
plt.show()