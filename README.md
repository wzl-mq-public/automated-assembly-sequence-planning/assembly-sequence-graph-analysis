# CAD-based assembly graph library

## Description
This repository contains multiple python packages for handling CAD-files within python.
Main applications are:
- **Collision data to and/or-graph** - Convert Moving Wedge and Liaison data to and/or graphs
- **Graph analysis** - Analyse precedence graphs & and/or-graphs alongside CAD models

## Installation

Software is programmed on Python 3.7. Functions are mainly built with python-occ (https://github.com/tpaviot/pythonocc-core).

Installation of all requirements with Anaconda3 is recommended:

> conda env create --name NAME --file environment.yml

## How to use the Project

Here are some descriptions for the most usages:

- data: Contains a sample step file. A sample file "centrifugal_pump.stp" from [GrabCAD](https://grabcad.com/library/centrifugal-pump-41) is given. 
- examples: Simple precedence graph generator. Loads assembly tiers pickle file, loads into networkX graph and plots it.
- src:
  - graph_utilities: Several scripts for graph generation and manipulation
  - validation: Scripts for validation of dissertation from Sören Münker
- tests: some experiments and unit tests of relevant functions

# Acknowledgements

This work is part of the research project “Internet of Construction” that is funded by the Federal Ministry of Education and Research of Germany within the indirective on a joint funding initiative in the field of innovation for production, services and labor of tomorrow (funding number: 02P17D081) and supported by the project management agency “Projektträger Karlsruhe (PTKA)”. The authors are responsible for the content.

This work has been executed at [WZL of RWTH Aachen Universits](https://wzl.rwth-aachen.de).
Main authors of the code: Sören Münker & Mikhail Polikarpov


