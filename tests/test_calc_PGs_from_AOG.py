from unittest import TestCase
from src.validation.evaluation_calculation import *
from src.graph_utilities.aog_to_assembly_tree import get_assembly_tree_random
from networkx.readwrite import json_graph

test_graph = {1: ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 6], [3]),
         2: ([1, 2, 4, 5, 6], [1, 2, 5, 6], [4]),
         3: ([1, 2, 4, 5, 6], [1, 4, 5], [2, 6]),
         4: ([1, 2, 5, 6], [1, 5], [2, 6]),
         5: ([1, 4, 5], [1, 5], [4]),
         6: ([1, 4, 5], [1, 4], [5]),
         7: ([1, 4], [1], [4]),
         8: ([1, 5], [1], [5]),
         9: ([2, 6], [2], [6])}

test_graph_2 = {
    1: ([1,2,3,4,5], [1,2], [3,4,5]),
    2: ([1,2,3,4,5], [1,2,3,4], [5]),
    3: ([1,2,3,4], [1,2,3], [4]),
    4: ([1,2,3], [1], [2,3]),
    5: ([1,2,3], [1,2], [3]),
    6: ([1,2], [1], [2]),
    7: ([2,3], [2], [3]),
    8: ([3,4,5], [3,4], [5]),
    9: ([3,4], [3], [4])
}

test_graph_3 = {
    1: ([1,2,3,4,5], [1,2,3,5], [4]),
    2: ([1,2,3,4,5], [1,2,3,4], [5]),
    3: ([1,2,3,5], [1,2,3], [5]),
    4: ([1,2,3,4], [1,2,3], [4]),
    5: ([1,2,3], [1], [2,3]),
    6: ([1,2,3], [2], [1,3]),
    7: ([2,3], [2], [3]),
    8: ([1,3], [2], [3])
}

test_graph_4 = {
    1: ([1,2,3,4,5,6], [1,2,3,5,6], [4]),
    2: ([1,2,3,4,5,6], [1,2,3,4], [5,6]),
    3: ([1,2,3,5,6], [1,2,3,5], [5,6]),
    4: ([1,2,3,4], [1,2,3], [4]),
    5: ([1,2,3,5], [1,2,3], [5]),
    6: ([1,2,3], [1], [2,3]),
    7: ([1,2,3], [2], [1,3]),
    8: ([2,3], [2], [3]),
    9: ([1,3], [2], [3]),
    10: ([5,6], [5], [6])
}

test_graph_5 = {1: ([1, 2, 3, 4, 5, 6, 7, 8], [1, 2, 4, 5, 6, 7, 8], [3]),
         2: ([1, 2, 4, 5, 6, 7, 8], [1, 2], [4, 5, 6, 7, 8]),
         3: ([1, 2], [1], [2]),
         4: ([4, 5, 6, 7, 8], [4], [5, 6, 7, 8]),
         5: ([4, 5, 6, 7, 8], [4, 7, 8], [5, 6]),
         6: ([4, 7, 8], [4], [7, 8]),
         7: ([4, 7, 8], [4, 7], [8]),
         8: ([5, 6, 7, 8], [5, 6], [7, 8]),
         9: ([5, 6], [5], [6]),
         10: ([4, 7], [4], [7]),
         11: ([7, 8], [7], [8])}


def get_num_unique_sets_from_random_choice(AOG, sample_size):
    random_PGs = []
    for i in range(0, sample_size):
        atg = get_assembly_tree_random(AOG)
        atg_data = json_graph.adjacency_data(atg)
        random_PGs.append(atg_data)

    unique_sets = [i for n, i in enumerate(random_PGs) if i not in random_PGs[n + 1:]]
    num_unique_sets = len(unique_sets)
    return num_unique_sets


class Test(TestCase):
    def test_graph(self):
        G = test_graph
        G_sorted = sort_by_values_len(G)
        possible_PG_count = calc_number_of_possible_PGs(G_sorted, plot_tree=True)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 100)
        self.assertTrue(num_unique_sets <= possible_PG_count)

    def test_graph2(self):
        G = test_graph_2
        G_sorted = sort_by_values_len(G)
        possible_PG_count = calc_number_of_possible_PGs(G_sorted, plot_tree=True)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 100)
        self.assertTrue(num_unique_sets <= possible_PG_count)

    def test_graph3(self):
        G = test_graph_3
        G_sorted = sort_by_values_len(G)
        possible_PG_count = calc_number_of_possible_PGs(G_sorted)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 100)
        self.assertTrue(num_unique_sets <= possible_PG_count)

    def test_graph4(self):
        G = test_graph_4
        G_sorted = sort_by_values_len(G)
        possible_PG_count = calc_number_of_possible_PGs(G_sorted)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 100)
        self.assertTrue(num_unique_sets <= possible_PG_count)

    def test_graph5(self):
        G = test_graph_5
        G_sorted = sort_by_values_len(G)
        possible_PG_count = calc_number_of_possible_PGs(G_sorted)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 100)
        self.assertTrue(num_unique_sets <= possible_PG_count)

    def test_case_centrifugal_pump(self):
        product_name = 'centrifugal_pump'
        pickle_path = '../data/and_or_pickles/' + product_name + '.pickle'
        with open(pickle_path, 'rb') as input_file:
            G = pickle.load(input_file)
        G_sorted = sort_by_values_len(G)

        possible_PG_count = calc_number_of_possible_PGs(G_sorted)
        num_unique_sets = get_num_unique_sets_from_random_choice(G, 2000)

        self.assertTrue(num_unique_sets <= possible_PG_count)

