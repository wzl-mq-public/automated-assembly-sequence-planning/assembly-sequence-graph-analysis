from OCC.Extend.DataExchange import read_step_file_with_names_colors
from OCC.Core.BRepClass3d import BRepClass3d_SolidExplorer

def main():
    step_file_path = '../data/stepFiles/centrifugal_pump.stp'
    aResShapeCompound = read_step_file_with_names_colors(step_file_path)

    se = BRepClass3d_SolidExplorer(list(aResShapeCompound)[0])
    tree = se.GetTree()
    print(tree)

if __name__ == '__main__':
    main()
    print("DONE")