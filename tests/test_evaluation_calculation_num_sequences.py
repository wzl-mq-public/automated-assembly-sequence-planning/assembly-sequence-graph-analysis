import unittest
import networkx as nx
from src.validation.evaluation_calculation import calc_number_of_possible_sequences

class TestCalcNumSequences(unittest.TestCase):
    def test_calc_number_of_possible_sequences_Ramos_case(self):
        g_example_ramos = nx.DiGraph()
        g_example_ramos.add_edge('Start', 0)
        g_example_ramos.add_edge(0, 1)
        g_example_ramos.add_edge(0, 9)
        g_example_ramos.add_edge(9, 10)
        g_example_ramos.add_edge(10, 11)
        g_example_ramos.add_edge(1, 2)
        g_example_ramos.add_edge(1, 7)
        g_example_ramos.add_edge(1, 8)
        g_example_ramos.add_edge(2, 3)
        g_example_ramos.add_edge(2, 5)
        g_example_ramos.add_edge(2, 6)
        g_example_ramos.add_edge(3, 4)
        g_example_ramos.add_edge(5, 4)
        g_example_ramos.add_edge(6, 4)

        num_sequences = calc_number_of_possible_sequences(g_example_ramos)

        self.assertTrue(num_sequences == 41580)

    def test_calc_number_of_possible_sequences_ex_1(self):
        g_example_1 = nx.DiGraph()
        g_example_1.add_edge('Start', 0)
        g_example_1.add_edge(0, 1)
        g_example_1.add_edge(1, 2)
        g_example_1.add_edge(1, 3)
        g_example_1.add_edge(2, 4)
        g_example_1.add_edge(3, 4)
        g_example_1.add_edge(0, 5)
        g_example_1.add_edge(5, 4)

        num_sequences = calc_number_of_possible_sequences(g_example_1)

        self.assertTrue(num_sequences == 8)
