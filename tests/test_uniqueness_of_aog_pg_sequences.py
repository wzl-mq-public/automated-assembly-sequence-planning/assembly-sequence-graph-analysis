from unittest import TestCase
import pickle
from src.validation.evaluation_calculation import sort_by_values_len
from src.validation.uniqueness_of_aog_pg_sequences import all_sequences_unique

test_graph = {1: ([1, 2, 3, 4, 5, 6], [1, 2, 4, 5, 6], [3]),
         2: ([1, 2, 4, 5, 6], [1, 2, 5, 6], [4]),
         3: ([1, 2, 4, 5, 6], [1, 4, 5], [2, 6]),
         4: ([1, 2, 5, 6], [1, 5], [2, 6]),
         5: ([1, 4, 5], [1, 5], [4]),
         6: ([1, 4, 5], [1, 4], [5]),
         7: ([1, 4], [1], [4]),
         8: ([1, 5], [1], [5]),
         9: ([2, 6], [2], [6])}

test_graph_2 = {
    1: ([1,2,3,4,5], [1,2], [3,4,5]),
    2: ([1,2,3,4,5], [1,2,3,4], [5]),
    3: ([1,2,3,4], [1,2,3], [4]),
    4: ([1,2,3], [1], [2,3]),
    5: ([1,2,3], [1,2], [3]),
    6: ([1,2], [1], [2]),
    7: ([2,3], [2], [3]),
    8: ([3,4,5], [3,4], [5]),
    9: ([3,4], [3], [4])
}

test_graph_3 = {
    1: ([1,2,3,4,5], [1,2,3,5], [4]),
    2: ([1,2,3,4,5], [1,2,3,4], [5]),
    3: ([1,2,3,5], [1,2,3], [5]),
    4: ([1,2,3,4], [1,2,3], [4]),
    5: ([1,2,3], [1], [2,3]),
    6: ([1,2,3], [2], [1,3]),
    7: ([2,3], [2], [3]),
    8: ([1,3], [2], [3])
}

test_graph_4 = {
    1: ([1,2,3,4,5,6], [1,2,3,5,6], [4]),
    2: ([1,2,3,4,5,6], [1,2,3,4], [5,6]),
    3: ([1,2,3,5,6], [1,2,3,5], [5,6]),
    4: ([1,2,3,4], [1,2,3], [4]),
    5: ([1,2,3,5], [1,2,3], [5]),
    6: ([1,2,3], [1], [2,3]),
    7: ([1,2,3], [2], [1,3]),
    8: ([2,3], [2], [3]),
    9: ([1,3], [2], [3]),
    10: ([5,6], [5], [6])
}

test_graph_5 = {1: ([1, 2, 3, 4, 5, 6, 7, 8], [1, 2, 4, 5, 6, 7, 8], [3]),
         2: ([1, 2, 4, 5, 6, 7, 8], [1, 2], [4, 5, 6, 7, 8]),
         3: ([1, 2], [1], [2]),
         4: ([4, 5, 6, 7, 8], [4], [5, 6, 7, 8]),
         5: ([4, 5, 6, 7, 8], [4, 7, 8], [5, 6]),
         6: ([4, 7, 8], [4], [7, 8]),
         7: ([4, 7, 8], [4, 7], [8]),
         8: ([5, 6, 7, 8], [5, 6], [7, 8]),
         9: ([5, 6], [5], [6]),
         10: ([4, 7], [4], [7]),
         11: ([7, 8], [7], [8])}

class TestUniqueness(TestCase):
    def test_all_sequences_unique_case1(self):
        all_unique = all_sequences_unique(test_graph)
        self.assertTrue(all_unique)

    def test_all_sequences_unique_case2(self):
        all_unique = all_sequences_unique(test_graph_2)
        self.assertTrue(all_unique)

    def test_all_sequences_unique_case3(self):
        all_unique = all_sequences_unique(test_graph_3, max_iter=500)
        self.assertTrue(all_unique)

    def test_all_sequences_unique_case4(self):
        all_unique = all_sequences_unique(test_graph_4, max_iter=500)
        self.assertTrue(all_unique)

    def test_all_sequences_unique_case5(self):
        all_unique = all_sequences_unique(test_graph_5, max_iter=500)
        self.assertTrue(all_unique)

    def test_all_sequences_unique_centrifugal_pump(self):
        product_name = 'centrifugal_pump'
        pickle_path = '../data/and_or_pickles/' + product_name + '.pickle'
        with open(pickle_path, 'rb') as input_file:
            G = pickle.load(input_file)
        G_sorted = sort_by_values_len(G)
        all_unique = all_sequences_unique(G_sorted, max_iter=500)
        self.assertTrue(all_unique)


