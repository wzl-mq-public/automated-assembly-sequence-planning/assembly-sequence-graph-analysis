import unittest
import networkx as nx
from src.validation.evaluation_calculation import calc_num_plans_of_parallel_node, calc_num_plans_of_serial_node

class TestCalc(unittest.TestCase):
    def test_calc_num_plans_of_parallel_node(self):
        test_tree = nx.DiGraph()
        test_tree.add_edge('p_0', 3)
        test_tree.add_edge('p_0', 5)

        num_plans = calc_num_plans_of_parallel_node(test_tree, 'p_0')

        self.assertTrue(num_plans == 2)

    def test_calc_num_plans_of_parallel_node2(self):
        test_tree2 = nx.DiGraph()
        test_tree2.add_edge('p_0', 3)
        test_tree2.add_edge('p_0', 5)
        test_tree2.add_edge('p_0', 6)

        num_plans = calc_num_plans_of_parallel_node(test_tree2, 'p_0')

        self.assertTrue(num_plans == 6)

    def test_calc_num_plans_of_serial_node(self):
        test_tree3 = nx.DiGraph()
        test_tree3.add_edge('p_0', 3)
        test_tree3.add_edge('p_0', 5)
        test_tree3.add_edge('p_0', 6)
        test_tree3.add_edge('s_0', 2)
        test_tree3.add_edge('s_0', 4)
        test_tree3.add_edge('s_0', 'p_0')

        num_plans = calc_num_plans_of_serial_node(test_tree3, 's_0')

        self.assertTrue(num_plans == 6)

    def test_calc_num_plans_of_parallel_node3(self):
        test_tree4 = nx.DiGraph()
        test_tree4.add_edge('p_0', 3)
        test_tree4.add_edge('p_0', 5)
        test_tree4.add_edge('p_0', 6)
        test_tree4.add_edge('s_0', 2)
        test_tree4.add_edge('s_0', 4)
        test_tree4.add_edge('s_0', 'p_0')
        test_tree4.add_edge('p_1', 7)
        test_tree4.add_edge('p_1', 8)
        test_tree4.add_edge('p_1', 's_0')


        num_plans = calc_num_plans_of_parallel_node(test_tree4, 'p_1')

        self.assertTrue(num_plans == 252)

