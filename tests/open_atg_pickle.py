import os
import pickle
import networkx as nx
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dir_path = os.path.join('C:\\', 'Users', '...')
    read_path = dir_path
    write_path = dir_path

    product_name = "ex_rq1_1_centrifugal_pump"

    atg_path = os.path.join(read_path, product_name + '_atg_from_aog_max_flex.pickle')
    with open(atg_path, 'rb') as input_file:
        atg = pickle.load(input_file)

    for node, data in atg.nodes(data=True):
        print(node, data)

    nx.draw(atg, with_labels=True)
    plt.show()

    aog_path = os.path.join(read_path, product_name + '_AOG.pickle')
    with open(aog_path, 'rb') as input_file:
        aog = pickle.load(input_file)

    print('AOG')
    print(aog)



