"""
For running from command line, building an app from top folder is needed.
References to singular packages and scripts might not work outside of Pycharm.
"""

from src.validation.rq1_validation_experiments import main

if __name__ == '__main__':
    main()